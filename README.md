# Xilinx Vitis Platforms

Personal collection of custom Vitis Embedded Platform recipes.

## References
- Platforms based on the Xilinx Official Platforms provided in the Xilinx [Vitis Embedded Platform Source](https://github.com/Xilinx/Vitis_Embedded_Platform_Source) repository.
- Download [Xilinx Common Images for Embedded Vitis Platforms](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-platforms.html)

## Quickstart Instructions
- For specific instructions for creating a platform with custom Petalinux BSP, see [README.custom.md](./README.custom.md)
- For specific instructions for creating a platform using a pre-built (common) BSP release, see [README.prebuilt.md](./README.prebuilt.md)

### Getting Started
- Example: v2021.2 ZCU106 Base Platform

1. Select a version, platform and change to that directory in the repository
```bash
~$ cd v2021.2/xilinx_zcu106_base
~/v2021.2/xilinx_zcu106_base$
```

2. If you are customizing the platform, view and customize variables in the platform ```config``` folder
```bash
v2021.2/xilinx_zcu106_base/config
├── buildvars.sh
├── gitignoredfiles.sh
└── petalinuxconfigs.sh
```

3. Run ```make help``` to view build options for that platform
```bash
Makefile Usage:
-----------------------
Top Level Build Targets
-----------------------
  All the make commands install platform to "platform_repo/$(PLATFORM_NAME)/export/\$(PLATFORM_NAME)"

  make all
      Command used to generate platform with petalinux. Source petalinux before running this command.
      This command builds all software components.

  make all PRE_SYNTH=FALSE
      Command used to generate platform with post-impl xsa using petalinux. Source petalinux before running this command.
      By default, PRE_SYNTH=TRUE.

  make all_prebuilt PREBUILT_LINUX_PATH=<path/to/common_sw/dir>
      Command used to generate platform with pre-built software components.

  make all_prebuilt PRE_SYNTH=FALSE PREBUILT_LINUX_PATH=<path/to/common_sw/dir>
      Command used to generate platform with post-impl xsa and pre-built software components.
      By default, PRE_SYNTH=TRUE.

-----------------------
Boot Image Generation
-----------------------

  make bootimage
      Command used to generate a boot image using petalinux.
      Run "make all PRE_SYNTH=FALSE" first.

  make bootimage_prebuilt PREBUILT_LINUX_PATH=<path/to/common_sw/dir>
      Command used to generate a boot image with pre-built software components using bootgen.
      Run "make all_prebuilt PRE_SYNTH=FALSE PREBUILT_LINUX_PATH=<path/to/common/sw/dir>" first.

-----------------------
Clean and Reset Targets
-----------------------

  make clean
      Run to clean the hardware, software and platform projects.
      This will not remove sdk, sysroot or yocto component artifacts from the working build.

  make clean_sysroot
      Run to remove a temporarily installed SDK from the working build.

  make reset_bsp
      Run to reset the BSP project back to a clean default.
      This executes "petalinux-build -x mrproper", removes the imported hardware design configuration
      and all customized yocto components from the project.

-----------------------
Build Information
-----------------------

  make pfminfo
      Run to display information about the platform project (for use with Vitis Tools).

  make imageinfo
      Run to display the platform image files (for creating bootable SD Cards).

  make sdkinfo
      Run to display the petalinux SDK installer files (for software development).

  make sysrootinfo
      Run to display the working SYSROOT (sdk installation) that can be used locally for software development.

-----------------------
GIT Project Management
-----------------------

  make git_ignore_changes
      Run to have GIT ignore changes to specific local project files that do not normally need saved.
      These files are typically:
      - Required by Xilinx tools to identify a valid project and track current build status.
      - Modified by the local build configuration.

  make git_restore_ignore_changes
      Run to have GIT restore tracking changes to specific local project files that do not normally need saved.
      These files are typically:
      - Required by Xilinx tools to identify a valid project and track current build status.
      - Modified by the local build configuration.

  make git_show_ignored_changes
      Run to display all files that GIT is ignoring changes to for the local build.
```

## Repository Structure

### A Note About GNU MAKE and BASH

The Vitis Platform recipes in this repository have been adapted from Xilinx's [Vitis Embedded Platform Recipes](https://github.com/Xilinx/Vitis_Embedded_Platform_Source).  The Xilinx recipes are structured using GNU MAKE to handle the multiple layers of build dependencies to build a complete platform from source.  Even though this is the correct/preferred way to structure a complete build, I find this approach difficult to work with when creating new platforms or explaining the sequence of various stages of the build process.

For training, porting and ease of use during recipe integration, recipes in this repository have been restructured to use BASH shell scripts to execute each build stage from the top level GNU Makefile.  These shell scripts can be executed directly to run build stages without using GNU MAKE.

### File Structure and Descriptions

#### Build Configuration Files
- See README files for the pre-built and custom flows for more information on configuration options in these files.
```bash
.
├── config
│ ├── buildvars.sh
│ ├── gitignoredfiles.sh
│ └── petalinuxconfigs.sh
```

#### Vivado Design Files
```bash
├── hw
│ ├── constrs
│ │ └── xilinx_zcu106_base.xdc
│ ├── ip
│ ├── README.hw
│ └── xsa_scripts
│     ├── checkip.tcl
│     ├── config.tcl
│     ├── dr.bd.tcl
│     ├── pfm_decls.tcl
│     ├── project.tcl
│     └── xsa.tcl
```

#### Top Level Makefile
```bash
├── Makefile
```

#### Vitis Platform Generation Script
```bash
├── platform
│ └── generate_platform.tcl
```

#### Platform README
```bash
├── README.md
```

#### Project Environment Setup Script
```bash
├── setenv.sh
```

#### Petalinux BSP Project
```bash
└── sw
    ├── petalinux
    │ ├── config.project
    │ └── project-spec
    │     ├── attributes
    │     ├── configs
    │     │ ├── busybox
    │     │ ├── config
    │     │ ├── init-ifupdown
    │     │ └── rootfs_config
    │     ├── hw-description
    │     │ └── metadata
    │     └── meta-user
    │         ├── conf
    │         ├── COPYING.MIT
    │         ├── README
    │         ├── recipes-apps
    │         ├── recipes-bsp
    │         └── recipes-kernel
```

#### Prebuilt Flow BSP Project Files
```bash
└── sw
    └── prebuilt_linux
        ├── bootgen.bif
        ├── dtb_generator.tcl
        └── user_dts
            ├── system-conf.dtsi
            └── system-user.dtsi
```

## Using Platforms Created From This Repository

### Create a bootable SD Card
- Generated images that can be used to create a bootable SD Card
```bash
$ make imageinfo
    ./scripts/show_image_info.sh
    ################################################
    # show_image_info.sh: Showing Image Information
    ################################################
    /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/build/platform
    ├── boot
    │ ├── BOOT.BIN
    ...
    │ └── system.dtb
    ├── filesystem
    │ ├── rootfs.ext4
    │ └── rootfs.tar.gz
    └── image
        ├── boot.scr
        └── Image

    3 directories, 10 files
```

- Example partioning of a 16GB SD Card
- Note: Partitions formatted using ```mkfs.vfat``` and ```mkfs.ext4``` commands
```bash
Disk /dev/sdf: 14.86 GiB, 15931539456 bytes, 31116288 sectors
Disk model: SD/MMC          
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x00000000

Device     Boot   Start      End  Sectors  Size Id Type
/dev/sdf1          2048  4196351  4194304    2G  b W95 FAT32
/dev/sdf2       4196352 31116287 26919936 12.9G 83 Linux
```

- Copy ```BOOT.BIN```, ```boot.scr``` and ```Image``` to the FAT32 partition
- Unroll the ```rootfs.tar.gz``` onto the Linux partition
