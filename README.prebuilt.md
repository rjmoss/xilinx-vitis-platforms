# Xilinx Vitis Platforms

Personal collection of customized Vitis Embedded Platform recipes.

## References
- Platforms based on the Xilinx Official Platforms provided in the Xilinx [Vitis Embedded Platform Source](https://github.com/Xilinx/Vitis_Embedded_Platform_Source) repository.

## Quickstart Prebuilt FLow
Examples using the v2021.2 [xilinx_zcu106_base](./v2021.2/xilinx_zcu106_base) platform.

### Set build configuration variables
- Build variables are set in ```config/buildvars.sh```
- You may modify the variable values in this file, or pass new values from the command line during execution of the build
- Variables with a preceeding colon (example: ```: ${PRE_SYNTH:=TRUE}```) can be overridden on the command line
- Key variables are:
    - ```PRE_SYNTH```
        - Set to ```FALSE``` to implement the Vivado design and generate a bitstream
        - Set to ```TRUE``` (default) to use a pre-synthesis design to create the Vitis Platform
    - ```PREBUILT_LINUX_PATH```
        - Set to a local path for the Common Images for Embedded Vitis Platforms
        - Download these images from the [Vitis Embedded Platforms](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-platforms.html) page on the Xilinx Download site

### Set Petalinux BSP configuration variables
- Note: The Petalinux BSP configuration file ```config/petalinuxconfigs.sh``` is not used in the prebuilt flow

### Build Sequence for using Prebuilt Linux Images
0. Set the top level directory
1. Generate the Vivado Hardware Design
2. Run the Device Tree Generator
3. Build the Vitis Platform
4. Show Platform Information
5. Optional: Generate target boot images
6. Optional: Show generated image information

#### Makefile Overview for the Prebuilt flow
- The prebuilt make sequence overview
```bash
$ source setenv.sh
$ make all_prebuilt PRE_SYNTH=FALSE PREBUILT_LINUX_PATH=/xilinx/local/platforms/2021.2/xilinx-zynqmp-common-v2021.2/
    ...
    all_prebuilt: hw sw_prebuilt platform_prebuilt pfminfo imageinfo

    hw:
        ${SCRIPT_DIR}/build_hw.sh

    sw_prebuilt:
        ${SCRIPT_DIR}/build_dtb.sh
        ${SCRIPT_DIR}/copy_sw_components_prebuilt.sh

    platform_prebuilt:
        ${SCRIPT_DIR}/copy_platform_components_prebuilt.sh
        ${SCRIPT_DIR}/build_platform.sh

    pfminfo:
        ${SCRIPT_DIR}/show_platform_info.sh

    imageinfo:
        ${SCRIPT_DIR}/show_image_info.sh
```

- Build the boot image for the target
```bash
$ make bootimage_prebuilt
    ...
    bootimage_prebuilt:
        ${SCRIPT_DIR}/extract_bitfile.sh
        ${SCRIPT_DIR}/build_bootimage_prebuilt.sh
```

#### Manual Build Walkthrough
0. Set the top level directory
```bash
$ source setenv.sh
    ################################################
    # : Setting Top Level Directory
    # TOP_DIR=/home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base
    ################################################
```

1. Generate the Vivado Hardware Design
```bash
$ make hw PRE_SYNTH=FALSE
    ${SCRIPT_DIR}/build_hw.sh
    ################################################
    # build_hw.sh: Building hardware project
    ################################################

    ****** Vivado v2021.2 (64-bit)
      **** SW Build 3367213 on Tue Oct 19 02:47:39 MDT 2021
      **** IP Build 3369179 on Thu Oct 21 08:25:16 MDT 2021
        ** Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.

    source xsa_scripts/xsa.tcl -notrace
    TCL SCRIPT ARGUMENTS:
    ---------------------
    PLATFORM      = xilinx_zcu106_base
    PLATFORM_NAME = xilinx_zcu106_base_2021_2
    BOARD_NAME    = zcu106
    VER           = 2021.2
    PRE_SYNTH     = FALSE
    ---------------------
    PATHS:
    ---------------------
    BUILD_DIR     = build
    CONSTRS_DIR   = constrs
    IP_DIR        = ip
    ---------------------
    ...
    INFO: [Vivado 12-4896] Successfully created Hardware Platform: /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/hw/build/hw.xsa
write_hw_platform: Time (s): cpu = 00:00:13 ; elapsed = 00:00:15 . Memory (MB): peak = 4155.348 ; gain = 71.906 ; free physical = 36598 ; free virtual = 294994
    INFO: [Common 17-206] Exiting Vivado at Tue Mar 15 10:40:40 2022...
```

2. Run the Device Tree Generator
```bash
$ make sw_prebuilt
    ${SCRIPT_DIR}/build_dtb.sh
    ################################################
    # build_dtb.sh: Generate a device tree (Xilinx DTG)
    ################################################
    Cloning into '/home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/build/tmp/dt'...
    ...
    ################################################
    # copy_sw_components_prebuilt.sh: Copy PREBUILT software components
    # PREBUILT_LINUX_PATH=/xilinx/local/platforms/2021.2/xilinx-zynqmp-common-v2021.2/
    ################################################
```

3. Build the Vitis Platform
```bash
$ make platform_prebuilt
    ${SCRIPT_DIR}/copy_platform_components_prebuilt.sh
    ################################################
    # copy_platform_components_prebuilt.sh: Copy prebuilt platform components
    # PREBUILT_LINUX_PATH=/xilinx/local/platforms/2021.2/xilinx-zynqmp-common-v2021.2/
    ################################################
    ${SCRIPT_DIR}/build_platform.sh
    ################################################
    # build_platform.sh: Building platform files
    ################################################
    ...
    xsa_path      : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/hw/build/hw.xsa
    platform_name : xilinx_zcu106_base_2021_2
    platform_desc : A basic platform targeting the ZCU106 evaluation board.  More information at https://www.xilinx.com/products/boards-and-kits/zcu106.html
    emu_xsa_path  : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/hw/build/hw_emu/hw_emu.xsa
    platform_out  : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/platform_repo
    boot_dir_path : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/build/platform/boot
    img_dir_path  : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/build/platform/image
    Opening the hardware design, this may take few seconds.
    INFO: [Hsi 55-2053] elapsed time for repository (/opt/tools/Xilinx/Vitis/2021.2/data/embeddedsw) loading 13 seconds
    INFO: Populating the default qemu data for the domain "xrt" from the install location /opt/tools/Xilinx/Vitis/2021.2/data/emulation/platforms/zynqmp/sw/a53_linux/qemu/
```

4. Show Platform Information
```bash
$ make pfminfo
${SCRIPT_DIR}/show_platform_info.sh
    ################################################
    # show_platform_info.sh: Showing Platform Information
    ################################################
    ==========================
    Basic Platform Information
    ==========================
    Platform:           xilinx_zcu106_base_2021_2
    File:               /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/platform_repo/xilinx_zcu106_base_2021_2/export/xilinx_zcu106_base_2021_2/xilinx_zcu106_base_2021_2.xpfm
    Description:        
    A basic platform targeting the ZCU106 evaluation board.  More information at https://www.xilinx.com/products/boards-and-kits/zcu106.html
        

    =====================================
    Hardware Platform (Shell) Information
    =====================================
    Vendor:                           xilinx.com
    Board:                            xilinx_zcu106_base_2021_2
    Name:                             xilinx_zcu106_base_2021_2
    Version:                          2021.2
    Generated Version:                2021.2
    Hardware:                         1
    Software Emulation:               1
    Hardware Emulation:               1
    Hardware Emulation Platform:      0
    FPGA Family:                      zynquplus
    FPGA Device:                      xczu7ev
    Board Vendor:                     xilinx.com
    Board Name:                       xilinx.com:zcu106:2.6
    Board Part:                       xczu7ev-ffvc1156-2-e

    =================
    Clock Information
    =================
      Default Clock Index: 0
      Clock Index:         0
        Frequency:         149.985000
      Clock Index:         1
        Frequency:         299.970000
      Clock Index:         2
        Frequency:         74.992500
      Clock Index:         3
        Frequency:         99.990000
      Clock Index:         4
        Frequency:         199.980000
      Clock Index:         5
        Frequency:         399.960000
      Clock Index:         6
        Frequency:         599.940000

    ==================
    Memory Information
    ==================
      Bus SP Tag: HP0
      Bus SP Tag: HP1
      Bus SP Tag: HP2
      Bus SP Tag: HP3
      Bus SP Tag: HPC0
      Bus SP Tag: HPC1
      Bus SP Tag: LPD

    =============================
    Software Platform Information
    =============================
    Number of Runtimes:            1
    Default System Configuration:  xilinx_zcu106_base_2021_2
    System Configurations:
      System Config Name:                      xilinx_zcu106_base_2021_2
      System Config Description:               xilinx_zcu106_base_2021_2
      System Config Default Processor Group:   xrt
      System Config Default Boot Image:        standard
      System Config Is QEMU Supported:         1
      System Config Processor Groups:
        Processor Group Name:      xrt
        Processor Group CPU Type:  cortex-a53
        Processor Group OS Name:   linux
      System Config Boot Images:
        Boot Image Name:           standard
        Boot Image Type:           
        Boot Image BIF:            xilinx_zcu106_base_2021_2/boot/linux.bif
        Boot Image Data:           xilinx_zcu106_base_2021_2/xrt/image
        Boot Image Boot Mode:      sd
        Boot Image RootFileSystem: 
        Boot Image Mount Path:     /mnt
        Boot Image Read Me:        xilinx_zcu106_base_2021_2/boot/generic.readme
        Boot Image QEMU Args:      xilinx_zcu106_base_2021_2/qemu/pmu_args.txt:xilinx_zcu106_base_2021_2/qemu/qemu_args.txt
        Boot Image QEMU Boot:      
        Boot Image QEMU Dev Tree:  
    Supported Runtimes:
      Runtime: OpenCL
```

5. Optional: Generate target boot images
```bash
$ make bootimage_prebuilt
    ${SCRIPT_DIR}/extract_bitfile.sh
    ################################################
    # extract_bitfile.sh: Extracting BIT file from XSA
    ################################################
    INFO: [Hsi 55-2053] elapsed time for repository (/opt/tools/Xilinx/Vitis/2021.2/data/embeddedsw) loading 0 seconds
    ${SCRIPT_DIR}/build_bootimage_prebuilt.sh
    ################################################
    # build_bootimage_prebuilt.sh: Building boot image (prebuilt_linux)
    ################################################


    ****** Xilinx Bootgen v2021.2
      **** Build date : Oct 19 2021-03:13:00
        ** Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.


    [INFO]   : Bootimage generated successfully
```

6. Optional: Show generated image information
```bash
$ make imageinfo
    ${SCRIPT_DIR}/show_image_info.sh
    ################################################
    # show_image_info.sh: Showing Image Information
    ################################################
    /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/build/platform
    ├── boot
    │ ├── bl31.elf
    │ ├── BOOT.BIN
    │ ├── fsbl.elf
    │ ├── pmufw.elf
    │ ├── system.dtb
    │ └── u-boot.elf
    ├── filesystem
    │ ├── rootfs.ext4
    │ └── rootfs.tar.gz
    └── image
        ├── boot.scr
        └── Image

    3 directories, 10 files
```