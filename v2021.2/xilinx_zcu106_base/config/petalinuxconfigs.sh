#!/bin/bash
# Bring in environment variables
#. ${TOP_DIR}/config/buildvars.sh

#Customize Petalinux BSP
if [[ -z $_ENV_PETALINUXCONFIGS_SH ]]; then
	export _ENV_PETALINUXCONFIGS_SH=1

	if [ -v _DEBUG ]; then echo "# Including: "$(basename ${BASH_SOURCE[0]}); fi

	#Get the current folder
	# Bring in environment variables
	#. $(dirname "${BASH_SOURCE[0]}")/buildvars.sh

	# Bring in environment variables
	. ${TOP_DIR}/config/buildvars.sh

	# Export all environment variables
	set -a

	# Configuration values are stored in arrays
	# 1.) set all three array values using current _cindex
	# _config_file[]: The file for the config to be added to
	# _config_param[]: The parameter string to replace
	# _config_value[]: The configuration value
	# 2.) increment _cindex
	# _cindex=_cindex+1

	declare -a _config_file
	declare -a _config_param
	declare -a _config_value
	_cindex=0

	# Configuration Parameters to update
	# Use readlink to normalize the file path
	# -m option because directory may not exist yet
	###############################################
	# YOCTO CONFIGS
	###############################################
	# CONFIG_TMP_DIR_LOCATION: temporary build artifacts
	_config_file[$_cindex]=${TOP_DIR}/sw/petalinux/project-spec/configs/config
	_config_param[$_cindex]=CONFIG_TMP_DIR_LOCATION
	_config_value[$_cindex]=$(readlink -m ${SW_DIR})
	#_config_value[$_cindex]=\${\{PROOT\}/build/tmp
	_cindex=$((_cindex+1))

	# CONFIG_SUBSYSTEM_TFTPBOOT_DIR: tftp boot server folder
	_config_file[$_cindex]=${TOP_DIR}/sw/petalinux/project-spec/configs/config
	_config_param[$_cindex]=CONFIG_SUBSYSTEM_TFTPBOOT_DIR
	_config_value[$_cindex]=/srv/tftpboot/v${PETALINUX_VER}/${PLATFORM}
	_cindex=$((_cindex+1))

	# CONFIG_YOCTO_BB_NUMBER_THREADS: number of processor threads bitbake can use
	_config_file[$_cindex]=${TOP_DIR}/sw/petalinux/project-spec/configs/config
	_config_param[$_cindex]=CONFIG_YOCTO_BB_NUMBER_THREADS
	_config_value[$_cindex]=16
	_cindex=$((_cindex+1))

	# CONFIG_YOCTO_PARALLEL_MAKE: number of parallel make instances
	_config_file[$_cindex]=${TOP_DIR}/sw/petalinux/project-spec/configs/config
	_config_param[$_cindex]=CONFIG_YOCTO_PARALLEL_MAKE
	_config_value[$_cindex]=8
	_cindex=$((_cindex+1))

	# CONFIG_YOCTO_LOCAL_SSTATE_FEEDS_URL
	_config_file[$_cindex]=${TOP_DIR}/sw/petalinux/project-spec/configs/config
	_config_param[$_cindex]=CONFIG_YOCTO_LOCAL_SSTATE_FEEDS_URL
	_config_value[$_cindex]=/xilinx/local/sstate-mirrors/sstate_aarch64_${PETALINUX_VER}/aarch64/
	_cindex=$((_cindex+1))

	###############################################
	# PETALINUX CONFIGS
	###############################################
	# DL_DIR: Common download directory
	_config_file[$_cindex]=${TOP_DIR}/sw/petalinux/project-spec/meta-user/conf/petalinuxbsp.conf
	_config_param[$_cindex]=DL_DIR
	_config_value[$_cindex]=/xilinx/local/downloads/${PETALINUX_VER}
	_cindex=$((_cindex+1))

	# SOURCE_MIRROR_URL: source mirror - use the download directory for this too
	_config_file[$_cindex]=${TOP_DIR}/sw/petalinux/project-spec/meta-user/conf/petalinuxbsp.conf
	_config_param[$_cindex]=SOURCE_MIRROR_URL
	_config_value[$_cindex]=file:///xilinx/local/downloads/${PETALINUX_VER}
	_cindex=$((_cindex+1))

	# SSTATE_DIR: Shared state director for ARMv8 architecture
	_config_file[$_cindex]=${TOP_DIR}/sw/petalinux/project-spec/meta-user/conf/petalinuxbsp.conf
	_config_param[$_cindex]=SSTATE_DIR
	_config_value[$_cindex]=/xilinx/local/sstate-cache/${PETALINUX_VER}
	_cindex=$((_cindex+1))

	# Turn off environment variable export
	set +a
else
	if [ -v _DEBUG ]; then
		echo "# WARNING: _ENV_PETALINUXCONFIGS_SH=${_ENV_PETALINUXCONFIGS_SH} "
		echo "# -------:  Skipping include: "$(basename ${BASH_SOURCE[0]})
	fi
fi