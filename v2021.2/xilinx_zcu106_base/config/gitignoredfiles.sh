#!/bin/bash
# Bring in environment variables
#. ${TOP_DIR}/config/buildvars.sh

#Generate a list of files to ignore changes to
if [[ -z $_ENV_GITIGNOREDFILES_SH ]]; then
	export _ENV_ENV_GITIGNOREDFILES_SH=1

	if [ -v _DEBUG ]; then echo "# Including: "$(basename ${BASH_SOURCE[0]}); fi

	#Get the current folder
	# Bring in environment variables
	#. $(dirname "${BASH_SOURCE[0]}")/buildvars.sh

	# Bring in environment variables
	. ${TOP_DIR}/config/buildvars.sh

	# Export all environment variables
	set -a

	# Configuration values are stored in arrays
	# 1.) set all array values using current _cindex
	# _ignored_files[]: The files in the repository for GIT to ignore changes to
	# 2.) increment _cindex
	# _gindex=_gindex+1

	declare -a _ignored_files

	_gindex=0

	# Files to ignore changes to
	###############################################
	# Petalinux Metadata - Required for Petalinux to "detect" a valid project
	_ignored_files[$_gindex]=${TOP_DIR}/sw/petalinux/.petalinux/metadata
	_gindex=$((_gindex+1))

	# Petalinux Yocto Configuration
	_ignored_files[$_gindex]=${TOP_DIR}/sw/petalinux/project-spec/configs/config
	_gindex=$((_gindex+1))

	# Petalinux BSP Configuration
	_ignored_files[$_gindex]=${TOP_DIR}/sw/petalinux/project-spec/meta-user/conf/petalinuxbsp.conf
	_gindex=$((_gindex+1))

	# Turn off environment variable export
	set +a
else
	if [ -v _DEBUG ]; then
		echo "# WARNING: _ENV_GITIGNOREDFILES_SH=${_ENV_GITIGNOREDFILES_SH} "
		echo "# -------:  Skipping include: "$(basename ${BASH_SOURCE[0]})
	fi
fi