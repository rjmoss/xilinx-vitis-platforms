# Vitis Platforms for the ZCU106 Board
The platforms in this repository are provided as building blocks for customizing you own platform on the ZCU106.  All platforms come with PetaLinux, OpenCV and include hooks for exercising Vitis Capabilities on the ZCU106 board.  Several platforms also combine key VCU TRD design components to enable a broad range of video-based capabilities.

Platforms in this repository follow a common build flow that is described in the remainder of this README.

## References
- [Petalinux v2021.2 Release Notes](https://support.xilinx.com/s/article/000032521?language=en_US)
- [ZCU106 v2021.2 Petalinux Release BSP](https://www.xilinx.com/member/forms/download/xef.html?filename=xilinx-zcu106-v2021.2-final.bsp)

## Platform List
- Base ZCU106 Vitis Platform
	- [xilinx_zcu106_base](./xilinx_zcu106_base/)
	- Includes Vitis Platform Hooks

- VCU Enabled ZCU106 Vitis Platform
	- [xilinx_zcu106_vcu](./xilinx_zcu106_vcu/)
	- Includes Vitis Platform Hooks
	- VCU Encode/Decode Support (4kp60)

- VCU Enabled ZCU106 Vitis Platform with HDMI Display
	- [xilinx_zcu106_vcu](./xilinx_zcu106_vcu_hdmitx/)
	- Includes Vitis Platform Hooks
	- VCU Encode/Decode Support (4kp60)
	- HDMI Transmit + Video Mixer with 8-layers

- VCU Enabled ZCU106 Vitis Platform with HDMI Display and Vitis AI 2.0 Support
	- [xilinx_zcu106_vcu_hdmitxrx](./xilinx_zcu106_vcu_hdmitx_vai/)
	- Includes Vitis Platform Hooks
	- Includes Vitis AI 2.0 Libraries
	- VCU Encode/Decode Support (4kp60)
	- HDMI Transmit + Video Mixer with 8-layers

- VCU Enabled ZCU106 Vitis Platform with HDMI Display and HDMI Input with Vitis AI 2.0 Support
	- [xilinx_zcu106_vcu_hdmitxrx](./xilinx_zcu106_vcu_hdmitxrx_vai/)
	- Includes Vitis Platform Hooks
	- VCU Encode/Decode Support (4kp60)
	- HDMI Transmit + Video Mixer with 8-layers
	- HDMI Receive (single stream, no broadcaster support)

## MPSoC Configuration
The MPSoC PS configuration are based on the ZCU106 board presets to accommodate the acceleration platform hooks and enable the VCU and other VCU TRD based design components.  Please see the Vivado block design and MPSoC configurations in Vivado for detailed information on each platform's configuration.

## Quickstart Full Build Sequence
- Overview
	- 1. Choose Pre or Post Synthesis platform build
	- 2. Build the Sysroot (SDK)
	- 3. Build boot images (post synthesis only)
	- 4. Create a bootable SD Card (post synthesis only)

- Pre-synthesis build
```bash
$ make all
```

- Post-Implementation build
```bash
$ make all PRE_SYNTH=FALSE
```

- Sysroot build
```bash
$ make sysroot
```

- Bootimage build (requires PRE_SYNTH=FALSE built first)
```bash
$ make bootimage
```

- Image files for SD Card:
```bash
sw/build/platform
├── BOOT.BIN
├── filesystem
│   ├── rootfs.ext4
│   └── rootfs.tar.gz
└── image
    ├── boot.scr
    ├── Image
    └── system.dtb

3 directories, 11 files
```

### Create a bootable SD Card for the Full Build Sequence
- Boot images can be found in the ```./sw/build/platform``` folder
- SD Card should have 2 partitions:
	- Partition #1: FAT32 W95 (Linux id `b`)
	- Partition #2: EXT4 Linux (Linux id `83`)

- Example sequence with:
	- 16GB SD Card as ```/dev/sde```
	- Temporary SD Card mount point as ```~/mnt/sdcard```
	- Temporary Rootfs mount point as ```~/mnt/rootfs```

- List partitions
```bash
$ sudo fdisk -l /dev/sde
	...
	Device     Boot   Start      End  Sectors  Size Id Type
	/dev/sde1          2048  2099199  2097152    1G  b W95 FAT32
	/dev/sde2       2099200 31116287 29017088 13.9G 83 Linux
```

- Mount SD Card boot partition
```bash
$ sudo mount /dev/sde1 ~/mnt/sdcard
```

## Quickstart Common Image Build Sequence
- Overview
	- 1. Choose Pre or Post Synthesis platform build
	- 2. Build boot images (post synthesis only)
	- 3. Create a bootable sd card (post synthesis only)

- Example common vitis image location: ```~/xilinx-zynqmp-common-v2021.2/```

- Pre-synthesis build
```bash
$ make all_prebuilt PREBUILT_LINUX_PATH=~/xilinx-zynqmp-common-v2021.2/
```

- Post-Implementation build
```bash
$ make all PRE_SYNTH=FALSE PREBUILT_LINUX_PATH=~/xilinx-zynqmp-common-v2021.2/
```

- Bootimage build (requires PRE_SYNTH=FALSE built first)
```bash
$ make bootimage_prebuilt PREBUILT_LINUX_PATH=~/xilinx-zynqmp-common-v2021.2/
```

- Image files for SD Card:
```bash
sw/build/platform
├── boot
│   ├── BOOT.BIN
│   └── system.dtb
└── image
    └── boot.scr

~/xilinx-zynqmp-common-v2021.2/
├── boot.scr
├── Image
├── rootfs.ext4
└── rootfs.tar.gz
```

### Create a bootable SD Card for the Common Build Sequence
- Boot images are located in two places
	- The bootloader (BOOT.BIN), bootloader script (boot.scr) and device tree (system.dtb) are compile and located in the ```./sw/build/platform``` folder
	- The common Kernel and rootfs images are located in the common image directory ```$PREBUILT_LINUX_PATH```
- SD Card should have 2 partitions:
	- Partition #1: FAT32 W95 (Linux id `b`)
	- Partition #2: EXT4 Linux (Linux id `83`)

- Example sequence with:
	- 16GB SD Card as ```/dev/sde```
	- Temporary SD Card mount point as ```~/mnt/sdcard```
	- Temporary Rootfs mount point as ```~/mnt/rootfs```

- List partitions
```bash
$ sudo fdisk -l /dev/sde
	...
	Device     Boot   Start      End  Sectors  Size Id Type
	/dev/sde1          2048  2099199  2097152    1G  b W95 FAT32
	/dev/sde2       2099200 31116287 29017088 13.9G 83 Linux
```

- Mount SD Card boot partition
```bash
$ sudo mount /dev/sde1 ~/mnt/sdcard
```

### Instructions using the full image build
- Copy boot files
```bash
$ sudo cp \
sw/build/platform/boot/BOOT.BIN \
sw/build/platform/image/boot.scr \
$PREBUILT_LINUX_PATH/Image \
sw/build/platform/boot/system.dtb \
~/mnt/sdcard
```

- Flush writes to disk and umount
```bash
$ sudo sync && sudo umount ~/mnt/sdcard
```

- Mount SD Card rootfs partition
```bash
$ sudo mount /dev/sde2 ~/mnt/sdcard
```

#### Example with ext4 rootfs image
- Mount the rootfs image file
```bash
$ sudo mount $PREBUILT_LINUX_PATH/rootfs.ext4 ~/mnt/rootfs -o loop
```

- Copy the roofts image contents to the rootfs partition on the SD Card
```bash
$ sudo cp -rf ~/mnt/rootfs/* ~/mnt/sdcard/
```

- Flush writes to disk and umount
```bash
$ sudo sync && sudo umount ~/mnt/sdcard && sudo umount ~/mnt/rootfs
```

#### Example with tar.gz rootfs image
- Decompress the roofts image contents to the rootfs partition on the SD Card
```bash
$ sudo tar -zxvf $PREBUILT_LINUX_PATH/rootfs.tar.gz -C ~/mnt/sdcard/
```

- Flush writes to disk and umount
```bash
$ sudo sync && sudo umount ~/mnt/sdcard
```

# Notes

Use the V++ -p option to generate the sd_card.img file that consists rootfs.ext4 provided by petalinux along with the Image,BOOT.BIN and system.dtb from platform, v++ generated xclbin and host.exe files.

Once the Vitis platform is ready, some example applications to build with these platforms can be found here:
https://github.com/Xilinx/Vitis_Accel_Examples
