# Copyright 2021 Xilinx Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#include platform.mk

############################## Help Section ##############################
.PHONY: help

help::
	@echo 'Makefile Usage:'
	@echo '-----------------------'
	@echo 'Top Level Build Targets'
	@echo '-----------------------'
	@echo '  All the make commands install platform to "platform_repo/$$(PLATFORM_NAME)/export/\$$(PLATFORM_NAME)"'
	@echo ''
	@echo '  make all'
	@echo '      Command used to generate platform with petalinux. Source petalinux before running this command.'
	@echo '      This command builds all software components.'
	@echo ''
	@echo '  make all PRE_SYNTH=FALSE'
	@echo '      Command used to generate platform with post-impl xsa using petalinux. Source petalinux before running this command.'
	@echo '      By default, PRE_SYNTH=TRUE.'
	@echo ''
	@echo '  make all_prebuilt PREBUILT_LINUX_PATH=<path/to/common_sw/dir>'
	@echo '      Command used to generate platform with pre-built software components.'
	@echo ''
	@echo '  make all_prebuilt PRE_SYNTH=FALSE PREBUILT_LINUX_PATH=<path/to/common_sw/dir>'
	@echo '      Command used to generate platform with post-impl xsa and pre-built software components.'
	@echo '      By default, PRE_SYNTH=TRUE.'
	@echo ''
	@echo '-----------------------'
	@echo 'Boot Image Generation'
	@echo '-----------------------'
	@echo ''
	@echo '  make bootimage'
	@echo '      Command used to generate a boot image using petalinux.'
	@echo '      Run "make all PRE_SYNTH=FALSE" first.'
	@echo ''
	@echo '  make bootimage_prebuilt PREBUILT_LINUX_PATH=<path/to/common_sw/dir>'
	@echo '      Command used to generate a boot image with pre-built software components using bootgen.'
	@echo '      Run "make all_prebuilt PRE_SYNTH=FALSE PREBUILT_LINUX_PATH=<path/to/common/sw/dir>" first.'
	@echo ''
	@echo '-----------------------'
	@echo 'Clean and Reset Targets'
	@echo '-----------------------'
	@echo ''
	@echo '  make clean'
	@echo '      Run to clean the hardware, software and platform projects.'
	@echo '      This will not remove sdk, sysroot or yocto component artifacts from the working build.'
	@echo ''
	@echo '  make clean_sysroot'
	@echo '      Run to remove a temporarily installed SDK from the working build.'
	@echo ''
	@echo '  make reset_bsp'
	@echo '      Run to reset the BSP project back to a clean default.'
	@echo '      This executes "petalinux-build -x mrproper", removes the imported hardware design configuration'
	@echo '      and all customized yocto components from the project.'
	@echo ''
	@echo '-----------------------'
	@echo 'Build Information'
	@echo '-----------------------'
	@echo ''
	@echo '  make pfminfo'
	@echo '      Run to display information about the platform project (for use with Vitis Tools).'
	@echo ''
	@echo '  make imageinfo'
	@echo '      Run to display the platform image files (for creating bootable SD Cards).'
	@echo ''
	@echo '  make sdkinfo'
	@echo '      Run to display the petalinux SDK installer files (for software development).'
	@echo ''
	@echo '  make sysrootinfo'
	@echo '      Run to display the working SYSROOT (sdk installation) that can be used locally for software development.'
	@echo ''
	@echo '-----------------------'
	@echo 'GIT Project Management'
	@echo '-----------------------'
	@echo ''
	@echo '  make git_ignore_changes'
	@echo '      Run to have GIT ignore changes to specific local project files that do not normally need saved.'
	@echo '      These files are typically:'
	@echo '      - Required by Xilinx tools to identify a valid project and track current build status.'
	@echo '      - Modified by the local build configuration.'
	@echo ''
	@echo '  make git_restore_ignore_changes'
	@echo '      Run to have GIT restore tracking changes to specific local project files that do not normally need saved.'
	@echo '      These files are typically:'
	@echo '      - Required by Xilinx tools to identify a valid project and track current build status.'
	@echo '      - Modified by the local build configuration.'
	@echo ''
	@echo '  make git_show_ignored_changes'
	@echo '      Run to display all files that GIT is ignoring changes to for the local build.'
	@echo ''	

############################## Target Section ##############################

# BUILD targets
.PHONY: all hw sw sw_prebuilt platform platform_prebuilt bootimage bootimage_prebuilt

all: checkenv hw sw_config sw platform pfminfo imageinfo

all_prebuilt: checkenv hw sw_prebuilt platform_prebuilt pfminfo imageinfo

hw: checkenv 
	${SCRIPT_DIR}/build_hw.sh

sw: checkenv
	${SCRIPT_DIR}/build_sw.sh
	${SCRIPT_DIR}/copy_sw_components.sh

sw_prebuilt: checkenv
	${SCRIPT_DIR}/build_dtb.sh
	${SCRIPT_DIR}/copy_sw_components_prebuilt.sh

platform: checkenv
	${SCRIPT_DIR}/copy_platform_components.sh
	${SCRIPT_DIR}/build_platform.sh

platform_prebuilt: checkenv
	${SCRIPT_DIR}/copy_platform_components_prebuilt.sh
	${SCRIPT_DIR}/build_platform.sh

# Boot Image Generation
.PHONY: bootimage bootimage_prebuilt
bootimage: checkenv
	${SCRIPT_DIR}/build_bootimage.sh
	${SCRIPT_DIR}/copy_bootimage.sh

bootimage_prebuilt: checkenv
	${SCRIPT_DIR}/extract_bitfile.sh
	${SCRIPT_DIR}/build_bootimage_prebuilt.sh

# GIT Repository Local Management
.PHONY: git_ignore_changes
git_ignore_changes: checkenv
	${SCRIPT_DIR}/git_ignore_changes.sh

.PHONY: git_restore_ignored_changes
git_restore_ignored_changes: checkenv
	${SCRIPT_DIR}/git_restore_ignored_changes.sh

.PHONY: git_show_ignored_changes
git_show_ignored_changes: checkenv
	${SCRIPT_DIR}/git_show_ignored_changes.sh

# Software development tool targets
.PHONY: sdk
sdk: checkenv
	${SCRIPT_DIR}/build_sdk.sh
	${SCRIPT_DIR}/show_sdk_info.sh

.PHONY: sysroot
sysroot: checkenv
	${SCRIPT_DIR}/build_sysroot.sh
	${SCRIPT_DIR}/show_sysroot_info.sh

# CLEAN targets
# NOTE: clean_sysroot is not run by default for the target `clean`
.PHONY: clean clean_hw clean_sw clean_platform clean_sysroot
clean: clean_hw clean_sw clean_platform

clean_hw: checkenv
	${SCRIPT_DIR}/clean_hw.sh

clean_sw: checkenv
	${SCRIPT_DIR}/clean_sw.sh

clean_platform: checkenv
	${SCRIPT_DIR}/clean_platform.sh

clean_sysroot: checkenv
	${SCRIPT_DIR}/clean_sysroot.sh

# RESET Petalinux BSP and remove all build artifcats using MRPROPER target
.PHONY: reset_bsp
reset_bsp: checkenv
	${SCRIPT_DIR}/reset_bsp.sh

# CONFIG targets
.PHONY: sw_config config_bsp config_xsa config_kernel config_rootfs
sw_config: config_bsp config_xsa config_kernel config_rootfs

config_bsp: checkenv
	${SCRIPT_DIR}/config_bsp.sh

config_xsa: checkenv
	${SCRIPT_DIR}/config_xsa.sh

config_kernel: checkenv
	${SCRIPT_DIR}/config_kernel.sh

config_rootfs: checkenv
	${SCRIPT_DIR}/config_rootfs.sh

config_xrt: checkenv
	${SCRIPT_DIR}/config_xrt.sh

config_zocl: checkenv
	${SCRIPT_DIR}/config_zocl.sh

# Informational Targets
.PHONY: pfminfo
pfminfo: checkenv
	${SCRIPT_DIR}/show_platform_info.sh

.PHONY: imageinfo
imageinfo: checkenv
	${SCRIPT_DIR}/show_image_info.sh

.PHONY: sdkinfo
sdkinfo: checkenv
	${SCRIPT_DIR}/show_sdk_info.sh

.PHONY: sysrootinfo
sysrootinfo: checkenv
	${SCRIPT_DIR}/show_sysroot_info.sh

############################## Envronment Section ##############################
# Require TOP_DIR and SCRIPT_DIR environment variables
.PHONY: checkenv
checkenv: env-TOP_DIR env-SCRIPT_DIR

# Require an environment variable to be set to run a makefile stage
# Example use:
# make_stage: env-VAR_NAME
.PHONY: env-%
env-%:
	@ if [ "${${*}}" = "" ];then \
		echo "Environment variable ${*} not set"; \
		exit 1; \
	fi