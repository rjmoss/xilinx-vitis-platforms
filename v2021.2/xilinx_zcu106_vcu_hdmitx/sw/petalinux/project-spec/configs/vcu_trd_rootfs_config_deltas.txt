# ROOTFS_CONFIG From VCU TRD Design:

CONFIG_kernel-module-hdmi=y
CONFIG_packagegroup-core-x11=y
CONFIG_v4l-utils=y
CONFIG_libv4l=y
CONFIG_media-ctl=y
CONFIG_yavta=y
CONFIG_gstreamer1.0-rtsp-server=y

Leaving this out:
# CONFIG_packagegroup-petalinux-qt=y

#
# modules 
#
CONFIG_kernel-module-pciep=y
CONFIG_board-fpga-autoload=y
CONFIG_custom-edid=y
