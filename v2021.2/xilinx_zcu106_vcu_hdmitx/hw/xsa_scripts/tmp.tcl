
  # Create address segments









  # Exclude Address Segments
  exclude_bd_addr_seg -offset 0xFF000000 -range 0x01000000 -target_address_space [get_bd_addr_spaces hdmi_output/v_frmbuf_rd_0/Data_m_axi_mm_video] [get_bd_addr_segs mpsoc_ss/ps_e/SAXIGP2/HP0_LPS_OCM]
  exclude_bd_addr_seg -offset 0xFF000000 -range 0x01000000 -target_address_space [get_bd_addr_spaces hdmi_output/v_mix_0/Data_m_axi_mm_video1] [get_bd_addr_segs mpsoc_ss/ps_e/SAXIGP2/HP0_LPS_OCM]
  exclude_bd_addr_seg -offset 0xFF000000 -range 0x01000000 -target_address_space [get_bd_addr_spaces hdmi_output/v_mix_0/Data_m_axi_mm_video2] [get_bd_addr_segs mpsoc_ss/ps_e/SAXIGP2/HP0_LPS_OCM]
  exclude_bd_addr_seg -offset 0xFF000000 -range 0x01000000 -target_address_space [get_bd_addr_spaces hdmi_output/v_mix_0/Data_m_axi_mm_video3] [get_bd_addr_segs mpsoc_ss/ps_e/SAXIGP2/HP0_LPS_OCM]
  exclude_bd_addr_seg -offset 0xFF000000 -range 0x01000000 -target_address_space [get_bd_addr_spaces hdmi_output/v_mix_0/Data_m_axi_mm_video4] [get_bd_addr_segs mpsoc_ss/ps_e/SAXIGP2/HP0_LPS_OCM]
  exclude_bd_addr_seg -offset 0xFF000000 -range 0x01000000 -target_address_space [get_bd_addr_spaces hdmi_output/v_mix_0/Data_m_axi_mm_video5] [get_bd_addr_segs mpsoc_ss/ps_e/SAXIGP2/HP0_LPS_OCM]
  exclude_bd_addr_seg -offset 0xFF000000 -range 0x01000000 -target_address_space [get_bd_addr_spaces hdmi_output/v_mix_0/Data_m_axi_mm_video6] [get_bd_addr_segs mpsoc_ss/ps_e/SAXIGP2/HP0_LPS_OCM]
  exclude_bd_addr_seg -offset 0xFF000000 -range 0x01000000 -target_address_space [get_bd_addr_spaces hdmi_output/v_mix_0/Data_m_axi_mm_video7] [get_bd_addr_segs mpsoc_ss/ps_e/SAXIGP2/HP0_LPS_OCM]
  exclude_bd_addr_seg -offset 0xFF000000 -range 0x01000000 -target_address_space [get_bd_addr_spaces hdmi_output/v_mix_0/Data_m_axi_mm_video8] [get_bd_addr_segs mpsoc_ss/ps_e/SAXIGP2/HP0_LPS_OCM]
  exclude_bd_addr_seg -target_address_space [get_bd_addr_spaces vitis_pfm/axi_vip_0/Master_AXI] [get_bd_addr_segs mpsoc_ss/ps_e/SAXIGP5/HP3_DDR_HIGH]
  exclude_bd_addr_seg -target_address_space [get_bd_addr_spaces vitis_pfm/axi_vip_1/Master_AXI] [get_bd_addr_segs mpsoc_ss/ps_e/SAXIGP6/LPD_DDR_HIGH]


  # Restore current instance
  current_bd_instance $oldCurInst

  # Create PFM attributes
  set_property PFM_NAME {xilinx.com:xd:xilinx_zcu106_vcu_hdmitx_2021_2:2021.2} [get_files [current_bd_design].bd]


  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


common::send_gid_msg -ssname BD::TCL -id 2053 -severity "WARNING" "This Tcl script was generated from a block design that has not been validated. It is possible that design <$design_name> may result in errors during validation."

