#!/bin/bash
#Environment from makefile
#Only import variables once
#Note: Defaults are set but can be overridden by setting them in the local environment
if [[ -z $_ENV_BUILDVARS_SH ]]; then
	export _ENV_BUILDVARS_SH=1

	if [ -v _DEBUG ]; then echo "# Including: "$(basename ${BASH_SOURCE[0]}); fi
	# Export all environment variables
	set -a

	# Configuration directorys
	CONFIG_DIR=config

	# Redirect command outputs for script debugging
	: ${REDIR_OUT:=/dev/null}

	#Local cache and download folders
	#https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842475/PetaLinux+Yocto+Tips#PetaLinuxYoctoTips-HowtoreducebuildtimeusingSSTATECACHE

	#tools
	VIVADO=${XILINX_VIVADO}/bin/vivado
	DTC=${XILINX_VITIS}/bin/dtc
	BOOTGEN=${XILINX_VITIS}/bin/bootgen
	XSCT=${XILINX_VITIS}/bin/xsct

	#platform specific
	: ${PLATFORM:=xilinx_zcu106_vcu_hdmitx_vai}
	: ${CPU_ARCH:=a53}
	: ${BOARD_NAME:=zcu106}
	: ${BOARD_REV:=revA}
	: ${BOARD:=${BOARD_NAME}-${BOARD_REV}}
	: ${CORE:=psu_cortexa53_0}

	PLATFORM_DESC='A basic platform with VCU encode/decode, HDMI Display support and Vitis AI 2.0, targeting the ZCU106 evaluation board.  More information at https://www.xilinx.com/products/boards-and-kits/zcu106.html'

	#versioning
	#note: VERSION is used in repo scripts to generate vitis platform names
	#      Use underscores in place of a period to avoid compilation problems
	: ${VERSION:=2021_2}
	#VERSION=2021.2
	: ${VER:=2021.2}

	#common
	#TOP_DIR set in top level setenv.sh script
	#TOP_DIR=$(readlink -f .)

	#hw related
	XSA_DIR=${TOP_DIR}/hw/build
	XSA=${XSA_DIR}/hw.xsa
	RP_XSA=${XSA_DIR}/rp/rp.xsa
	STATIC_XSA=${XSA_DIR}/static.xsa
	#Simulation with designs using the VCU is not supported
	#See PG252: Simulation, Page 95/379
	#https://www.xilinx.com/support/documentation/ip_documentation/vcu/v1_2/pg252-vcu.pdf
	: ${EMU_SUPPORT:=FALSE}
	#If this design can support hardware emulation, generate a hw_emu.xsa
	#: ${EMU_SUPPORT:=TRUE}
	HW_EMU_XSA=${XSA_DIR}/hw_emu/hw_emu.xsa
	: ${PRE_SYNTH:=TRUE}


	#sw related
	SW_DIR=${TOP_DIR}/sw/build
	SW_PLATFORM_DIR=${SW_DIR}/platform
	BOOT_DIR=${SW_DIR}/platform/boot
	IMAGE_DIR=${SW_DIR}/platform/image
	DTB_FILE=${BOOT_DIR}/system.dtb
	BOOT_IMAGE=${BOOT_DIR}/BOOT.BIN
	#SW_FILES=${IMAGE_DIR}/boot.scr ${BOOT_DIR}/u-boot.elf ${BOOT_DIR}/bl31.elf
	#BOOT_FILES=u-boot.elf bl31.elf
	SYSROOT=${TOP_DIR}/sw/petalinux/sysroot
	ZOCL_AUTO_GENERATE=true

	#platform related
	PLATFORM_NAME=${PLATFORM}_${VERSION}
	PLATFORM_SW_SRC=${TOP_DIR}/platform
	PLATFORM_DIR=${TOP_DIR}/platform_repo

	#prebuilt platform information
	: ${PREBUILT_LINUX_PATH:=/xilinx/local/platforms/2021.2/xilinx-zynqmp-common-v2021.2/}
	SYSTEM_USER_DTSI=${TOP_DIR}/sw/prebuilt_linux/user_dts/system-user.dtsi
	SYSTEM_CONF_DTS=${TOP_DIR}/sw/prebuilt_linux/user_dts/system-conf.dtsi

	# Turn off environment variable export
	set +a

else
	if [ -v _DEBUG ]; then
		echo "# WARNING: _ENV_BUILDVARS_SH=${_ENV_BUILDVARS_SH} "
		echo "# -------:  Skipping include: "$(basename ${BASH_SOURCE[0]})
	fi
fi