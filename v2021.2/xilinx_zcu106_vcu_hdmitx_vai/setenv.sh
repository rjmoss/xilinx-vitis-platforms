#!/bin/bash
# Environment Setup Script for vitis platform projects
_self=$(basename ${BASH_SOURCE[0]})

##########################################################################
# Modify the environment variables and paths configured below
##########################################################################
# Optional: Setup Vitis environment
#source /opt/tools/Xilinx/Vitis/2021.2/settings64.sh
# Optional: Setup the Petalinux environment
#source /opt/tools/Xilinx/Petalinux/2021.2/settings.sh
# Add CMAKE 3.22 to the path
#export PATH=/xilinx/local/tools/cmake/3.22/bin/:$PATH

# ##########################################################################
# # End of modifiable variables - the remaining are derived for the project
# ##########################################################################

export TOP_DIR=$(readlink -f $(dirname "${BASH_SOURCE[0]}"))
export SCRIPT_DIR=$(realpath ${TOP_DIR}/../scripts)
echo "################################################"
echo "# ${_self}: Setting Top Level Directory"
echo "# TOP_DIR=${TOP_DIR}"
echo "# ${_self}: Setting Script Directory"
echo "# SCRIPT_DIR=${SCRIPT_DIR}"
echo "################################################"
