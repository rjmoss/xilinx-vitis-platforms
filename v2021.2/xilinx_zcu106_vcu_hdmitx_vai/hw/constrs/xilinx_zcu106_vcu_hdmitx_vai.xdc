#(C) Copyright 2020 - 2021 Xilinx, Inc.
#SPDX-License-Identifier: Apache-2.0
#-------------------------------------------------------------------

#####
## Constraints for ZCU106
## Version 1.0
#####


#####
## Pins
#####
set_property PACKAGE_PIN AH12 [get_ports si570_user_clk_p]
set_property IOSTANDARD LVDS [get_ports si570_user_clk_p]

set_clock_groups -name mux_clk -logically_exclusive -group [get_clocks [list  [get_clocks -of_objects [get_pins bd_i/clk_wiz_1/inst/mmcme4_adv_inst/CLKOUT1]]]] -group [get_clocks clk_pl_2]

# HDMI TX
set_property PACKAGE_PIN AD8 [get_ports TX_REFCLK_P]

set_property PACKAGE_PIN G21 [get_ports HDMI_TX_CLK_P]
set_property IOSTANDARD LVDS [get_ports HDMI_TX_CLK_P]

set_property PACKAGE_PIN N13 [get_ports TX_HPD]
set_property IOSTANDARD LVCMOS33 [get_ports TX_HPD]

set_property PACKAGE_PIN N8 [get_ports TX_DDC_scl_io]
set_property IOSTANDARD LVCMOS33 [get_ports TX_DDC_scl_io]

set_property PACKAGE_PIN N9 [get_ports TX_DDC_sda_io]
set_property IOSTANDARD LVCMOS33 [get_ports TX_DDC_sda_io]

# I2C
set_property PACKAGE_PIN N12 [get_ports HDMI_CTRL_IIC_scl_io]
set_property IOSTANDARD LVCMOS33 [get_ports HDMI_CTRL_IIC_scl_io]

set_property PACKAGE_PIN P12 [get_ports HDMI_CTRL_IIC_sda_io]
set_property IOSTANDARD LVCMOS33 [get_ports HDMI_CTRL_IIC_sda_io]

# Misc
set_property PACKAGE_PIN H8 [get_ports {SI5319_RST[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SI5319_RST[0]}]

set_property PACKAGE_PIN G8 [get_ports SI5319_LOL]
set_property IOSTANDARD LVCMOS33 [get_ports SI5319_LOL]

set_property PACKAGE_PIN N11 [get_ports {TX_EN[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {TX_EN[0]}]

# Misc
#GPIO_LED_0_LS
set_property PACKAGE_PIN AL11 [get_ports LED0]
#GPIO_LED_1_LS
#set_property PACKAGE_PIN AL13 [get_ports {LED1[0]}]
#GPIO_LED_2_LS
#set_property PACKAGE_PIN AK13 [get_ports {LED2[0]}]
#GPIO_LED_3_LS
#set_property PACKAGE_PIN AE15 [get_ports {LED3[0]}]
#GPIO_LED_4_LS
#set_property PACKAGE_PIN AM8 [get_ports {LED4[0]}]
#GPIO_LED_5_LS
#set_property PACKAGE_PIN AM9 [get_ports {LED5[0]}]
#GPIO_LED_6_LS
set_property PACKAGE_PIN AM10 [get_ports LED6]
#GPIO_LED_7_LS
#set_property PACKAGE_PIN AM11 [get_ports LED7]

#Common LED
set_property IOSTANDARD LVCMOS12 [get_ports LED*]

create_clock -name TX_REFCLK_P -period 3.367 [get_ports TX_REFCLK_P]
