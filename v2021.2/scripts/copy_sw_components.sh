#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}//config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}//config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}: Copy software components"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
pushd ${TOP_DIR}/sw/petalinux >> $REDIR_OUT
mkdir -p ${BOOT_DIR} ${IMAGE_DIR}
cp -f images/linux/boot.scr ${IMAGE_DIR}/boot.scr
cp -f images/linux/bl31.elf ${BOOT_DIR}/bl31.elf
cp -f images/linux/system.dtb ${BOOT_DIR}/system.dtb
cp -f images/linux/u-boot.elf ${BOOT_DIR}/u-boot.elf
cp -f images/linux/pmufw.elf ${BOOT_DIR}/pmufw.elf
cp -f images/linux/zynqmp_fsbl.elf images/linux/fsbl.elf
cp -f images/linux/zynqmp_fsbl.elf ${BOOT_DIR}/fsbl.elf
cp -f images/linux/system.dtb ${IMAGE_DIR}/system.dtb
popd >> $REDIR_OUT
if [ -v _DEBUG ]; then set +x; fi
