#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}/config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}: Copy PREBUILT software components"
echo "# PREBUILT_LINUX_PATH=${PREBUILT_LINUX_PATH}"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
pushd ${TOP_DIR}/sw/prebuilt_linux >> $REDIR_OUT
mkdir -p ${IMAGE_DIR}
cp -f ${PREBUILT_LINUX_PATH}/boot.scr ${IMAGE_DIR}/boot.scr
cp -f ${PREBUILT_LINUX_PATH}/bl31.elf ${BOOT_DIR}/bl31.elf
cp -f ${PREBUILT_LINUX_PATH}/u-boot.elf ${BOOT_DIR}/u-boot.elf
popd >> $REDIR_OUT
if [ -v _DEBUG ]; then set +x; fi