#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh

echo "################################################"
echo "# ${_self}: Cleaning platform"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
pushd ${TOP_DIR} >> $REDIR_OUT
rm -rf ${PLATFORM_DIR}
popd >> $REDIR_OUT
if [ -v _DEBUG ]; then set +x; fi
