#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}/config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}: Showing Platform Information"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
pushd ${PLATFORM_DIR}/${PLATFORM_NAME}/export/${PLATFORM_NAME} >> $REDIR_OUT
if [ -f ${PLATFORM_NAME}.xpfm ]; then
	platforminfo ${PLATFORM_NAME}.xpfm
else
	echo "-- ${PLATFORN_NAME}.xpfm NOT FOUND!"
	echo "-- See the top level Makefile for help building and exporting the Vitis Platform."
fi
popd >> $REDIR_OUT
if [ -v _DEBUG ]; then set +x; fi
