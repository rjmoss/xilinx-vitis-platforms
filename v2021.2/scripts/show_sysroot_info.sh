#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}/config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}: Showing SYSROOT Location"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
if [ -d ${SYSROOT} ]; then
	tree -L 1 ${SYSROOT}
else
	echo "-- the ${SYSROOT} folder does not exist."
	echo "-- See the top level Makefile for help installing a SYSROOT from a Petalinux SDK for local software development with this platform."
fi
if [ -v _DEBUG ]; then set +x; fi
