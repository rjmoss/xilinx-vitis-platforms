#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh

echo "################################################"
echo "# ${_self}: Building hardware project"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
TCL_SOURCE=xsa_scripts/xsa.tcl
pushd ${TOP_DIR}/hw >> $REDIR_OUT
${VIVADO} -mode batch -notrace -source ${TCL_SOURCE} -tclargs ${PLATFORM} ${PLATFORM_NAME} ${BOARD_NAME} ${VER} ${EMU_SUPPORT} ${PRE_SYNTH}
popd >> $REDIR_OUT
if [ -v _DEBUG ]; then set +x; fi
