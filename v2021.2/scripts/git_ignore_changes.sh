#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
. ${TOP_DIR}/config/gitignoredfiles.sh

# Setup GIT ignores for local changes on specific project files
# See: git update-index documentation:
# - https://git-scm.com/docs/git-update-index#Documentation/git-update-index.txt---no-skip-worktree
# - https://git-scm.com/docs/git-update-index#_skip_worktree_bit

echo "################################################"
echo "# ${_self}:"
echo "# GIT skip-worktree Setup"
echo "# Ignoring changes to the following local files"
echo "# ---------------------------------------------"

if [ -v _DEBUG ]; then set -x; fi
pushd ${TOP_DIR} >> $REDIR_OUT
# Apply petalinux configurations
for (( i=0; i < _gindex; i++ )); do
	echo "# GIT skip-worktree: Ignoring $((i+1)) of ${_gindex}"
	echo "# - ${_ignored_files[$i]}"
	# Update the configuration file
	git update-index --skip-worktree ${_ignored_files[$i]}
done
echo "################################################"
popd >> $REDIR_OUT
git ls-files ${TOP_DIR} -v | grep ^S
echo "################################################"
if [ -v _DEBUG ]; then set +x; fi

# Petalinux Metadata - required to identify a valid "project"
#git update-index --skip-worktree ${TOP_DIR}/sw/petalinux/.petalinux/metadata
# Petalinux project config files setup with local build paths and machine configuration

#git update-index --skip-worktree ${TOP_DIR}/sw/petalinux/project-spec/configs/config
#git update-index --skip-worktree ${TOP_DIR}/sw/petalinux/project-spec/meta-user/conf/petalinuxbsp.conf


