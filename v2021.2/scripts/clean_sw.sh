#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}/config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}: Cleaning software project"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
pushd ${TOP_DIR}/sw/petalinux >> $REDIR_OUT
petalinux-build -x cleanall
if [ -f project-spec/hw-description/*.xsa ]; then
	rm -f project-spec/hw-description/*.xsa project-spec/hw-description/*.pdi project-spec/hw-description/*.bit
fi
rm -rf ${SW_DIR}
popd >> $REDIR_OUT
if [ -v _DEBUG ]; then set +x; fi
