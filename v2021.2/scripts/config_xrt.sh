#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh

echo "################################################"
echo "# ${_self}: Configuring XRT"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
pushd ${TOP_DIR}/sw/petalinux >> $REDIR_OUT
petalinux-config -c xrt --silentconfig
popd >> $REDIR_OUT
if [ -v _DEBUG ]; then set +x; fi
