#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}/config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}: Building boot image"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
pushd ${TOP_DIR}/sw/petalinux >> $REDIR_OUT
if [ -f project-spec/hw-description/hw.bit ]; then
	petalinux-package --force \
	--boot \
	--fsbl images/linux/fsbl.elf \
	--fpga project-spec/hw-description/hw.bit \
	--u-boot images/linux/u-boot.elf \
	--pmufw images/linux/pmufw.elf \
	--atf images/linux/bl31.elf
else
	echo "WARNING: \"hw.bit\" file not found. Skipping BOOT image generation."
fi
popd >> $REDIR_OUT
if [ -v _DEBUG ]; then set +x; fi
