#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}/config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}: Building Custom Sysroot"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
pushd ${TOP_DIR}/sw/petalinux >> $REDIR_OUT
mkdir -p ${SYSROOT}
petalinux-package --sysroot -d ${SYSROOT}
popd >> $REDIR_OUT
if [ -v _DEBUG ]; then set +x; fi
