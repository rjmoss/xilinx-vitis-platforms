#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Setup GIT ignores for local changes on specific project files
# See: git update-index documentation:
# - https://git-scm.com/docs/git-update-index#Documentation/git-update-index.txt---no-skip-worktree
# - https://git-scm.com/docs/git-update-index#_skip_worktree_bit

echo "################################################"
echo "# ${_self}: GIT skip-worktree Status"
echo "# Changes to the following local files are ignored"
echo "# ---------------------------------------------"
git ls-files ${TOP_DIR} -v | grep ^S
echo "################################################"
echo "# Note: To revert the skip-worktree status, use:"
echo "# git update-index --no-skip-worktree <file_name>"
echo "################################################"
