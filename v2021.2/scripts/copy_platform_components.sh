#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}/config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}: Copy platform components"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
pushd ${TOP_DIR}/sw/petalinux >> $REDIR_OUT
cp -f images/linux/Image ${SW_DIR}/platform/image/Image
mkdir -p ${SW_DIR}/platform/filesystem
cp -f images/linux/rootfs.tar.gz ${SW_DIR}/platform/filesystem/rootfs.tar.gz
cp -f images/linux/rootfs.ext4 ${SW_DIR}/platform/filesystem/rootfs.ext4
popd >> $REDIR_OUT
if [ -v _DEBUG ]; then set +x; fi
