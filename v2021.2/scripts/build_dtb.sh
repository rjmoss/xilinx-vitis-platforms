#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}/config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}: Generate a device tree (Xilinx DTG)"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
pushd ${TOP_DIR}/sw/prebuilt_linux >> $REDIR_OUT
mkdir -p ${SW_DIR}/tmp/dt
mkdir -p ${BOOT_DIR}
git clone https://github.com/Xilinx/device-tree-xlnx.git -b xlnx_rel_v${VER} ${SW_DIR}/tmp/dt
${XSCT} -nodisp dtb_generator.tcl ${PLATFORM} ${SW_DIR}/tmp/dt ${XSA} ${BOARD} ${CORE} ${SW_DIR}/tmp ${ZOCL_AUTO_GENERATE}
BSP_PATH=${SW_DIR}/tmp/Workspace/${PLATFORM}/${CORE}/device_tree_domain/bsp/
BOOT_ARTIFACTS=${SW_DIR}/tmp/Workspace/${PLATFORM}/export/${PLATFORM}/sw/${PLATFORM}/boot
cp -rf ${SYSTEM_CONF_DTS} ${BSP_PATH}
cp -rf ${SYSTEM_USER_DTSI} ${BSP_PATH}
if [ -f ${SYSTEM_USER_DTSI} ]; then echo "#include \"system-user.dtsi\"" >> ${BSP_PATH}/system-top.dts; fi
popd >> $REDIR_OUT
pushd ${BSP_PATH} >> $REDIR_OUT
cpp -Iinclude -E -P -undef -D__DTS__ -x assembler-with-cpp ./system-top.dts | ${DTC} -I dts -O dtb -o ${DTB_FILE} - && cd -
cp -rf ${BOOT_ARTIFACTS}/*.elf ${SW_DIR}/platform/boot/ 2>/dev/null || :
#rm -rf ${SW_DIR}/tmp
popd >> $REDIR_OUT
if [ -v _DEBUG ]; then set +x; fi
