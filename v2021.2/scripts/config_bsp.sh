#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}/config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}: Configuring BSP"
echo "################################################"

# Generic script to apply configuration file changes
CONFIG_SCRIPT=${SCRIPT_DIR}/helper/update_plnx_config.sh

if [ -v _DEBUG ]; then set -x; fi
pushd ${TOP_DIR}/sw/petalinux >> $REDIR_OUT
# Apply petalinux configurations
for (( i=0; i < _cindex; i++ )); do
	echo "################################################"
	echo "# Update: $((i+1)) of ${_cindex}"
	echo "################################################"
	# Update the configuration file
	${CONFIG_SCRIPT} ${_config_file[$i]} ${_config_param[$i]} ${_config_value[$i]}
done
popd >> $REDIR_OUT
if [ -v _DEBUG ]; then set +x; fi
