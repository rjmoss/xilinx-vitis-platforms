#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}/config/petalinuxconfigs.sh 

echo "################################################"
echo "# ${_self}: Extracting BIT file from XSA"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
mkdir -p ${SW_DIR}
cp -f ${XSA} ${SW_DIR}/system.xsa
#echo "openhw ${SW_DIR}/system.xsa" >> ${SW_DIR}/extract
echo "openhw ${SW_DIR}/system.xsa" > ${SW_DIR}/extract
${XSCT} ${SW_DIR}/extract
if [ -v _DEBUG ]; then set +x; fi
