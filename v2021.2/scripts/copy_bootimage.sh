#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}/config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}: Copy boot image"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
pushd ${TOP_DIR}/sw/petalinux >> $REDIR_OUT
if [ -f images/linux/BOOT.BIN ]; then
	mkdir -p ${BOOT_DIR}
	cp -rf images/linux/BOOT.BIN ${BOOT_DIR}
else
	echo "WARNING: \"BOOT.BIN\" file not found. Skipping BOOT image copy."
fi	
popd >> $REDIR_OUT
if [ -v _DEBUG ]; then set +x; fi
