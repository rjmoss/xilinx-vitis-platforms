#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh

echo "################################################"
echo "# ${_self}: Cleaning hardware project"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
# Clean the hardware project
pushd ${TOP_DIR}/hw >> $REDIR_OUT
rm -rf *.jou *.log build 
#rm -r vivado* build .Xil *dynamic* *.log *.xpe 
popd >> $REDIR_OUT
if [ -v _DEBUG ]; then set +x; fi
