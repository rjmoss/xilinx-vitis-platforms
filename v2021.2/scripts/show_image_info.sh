#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}/config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}: Showing Image Information"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
if [ -d ${SW_PLATFORM_DIR} ]; then
	tree ${SW_PLATFORM_DIR}
else
	echo "-- ${SW_PLATFORM_DIR} NOT FOUND!"
	echo "-- See the top level Makefile for help building the Platform Software Images."
fi
if [ -v _DEBUG ]; then set +x; fi
