#!/bin/bash
#_self="${0##*/}"
#_self=$(basename "$0")
_self=$(basename ${BASH_SOURCE[0]})

echo "# ${_self}"
echo "################################################"

# Process Command line arguments
if [[ $# -ne 3 ]]; then
	echo "${_self}"
	echo "ERROR: $# arguments were provided"
	echo "1: ${1}"
	echo "2: ${2}"
	echo "3: ${3}"
	echo "This scripts requires three arguments"
	echo "Syntax:"
	echo "$_self <petalinux_config_file> <config_variable> <desired_value>"
	echo "Examples:"
	echo "$_self project-spec/meta-user/configs/config CONFIG_TMP_DIR_LOCATION /tmp/xilinx_zcu106_base_2021.2"
	echo "-----------------------------------------"
	exit 1
else
	CONFIG_FILE=${1}
	KEY=${2}
	CONFIG_VALUE=${3}
	REPLACE="$KEY=\"${CONFIG_VALUE}\""

	if [ ! -f "${1}" ]; then
		echo "Petalinux Config File $1 Not Found."
		exit 1
	fi

	echo -e "\tFile      : "$(basename ${CONFIG_FILE})
	echo -e "\tLocation  : "$(dirname ${CONFIG_FILE})
	echo -e "\tKey       : "${KEY}
	#echo "Updating ${KEY} in $(basename ${CONFIG_FILE})"
	# Test for existing configuration entry
	if [ -v _DEBUG ]; then set -x; fi

	if grep -Fq $KEY $CONFIG_FILE; then
		# Replace existing configuration line
		echo -e "\tOLD value :" $(cat $CONFIG_FILE | grep $KEY)
		sed -i "s@^$KEY=.*@$REPLACE@g" $CONFIG_FILE
		echo -e "\tNEW value :" $(cat $CONFIG_FILE | grep $KEY)
	else
		# Add new string
		echo $REPLACE >> $CONFIG_FILE
		echo -e "\tNEW value :" $(cat $CONFIG_FILE | grep $KEY)
	fi	
	if [ -v _DEBUG ]; then set +x; fi
fi
