#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}/config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}: Resetting the Petalinux BSP"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
pushd ${TOP_DIR}/sw/petalinux >> $REDIR_OUT
petalinux-build -x mrproper -f
if [ -d project-spec/hw-description/ ]; then
	# Remove all hardware description files except metadata
	find project-spec/hw-description/ -type f ! -name "metadata" -exec rm -rf {} \;
fi
if [ -d components/ ]; then
	# Remove the yocto components folder
	rm -rf components/
fi
if [ -v _DEBUG ]; then set +x; fi
