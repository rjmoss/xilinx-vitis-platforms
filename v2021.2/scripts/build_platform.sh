#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}/config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}: Building platform files"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
pushd ${TOP_DIR}/platform >> $REDIR_OUT
${XSCT} -nodisp -sdx ${PLATFORM_SW_SRC}/generate_platform.tcl platform_name "${PLATFORM_NAME}" platform_desc "${PLATFORM_DESC}" xsa_path "${XSA}" emu_xsa_path "${HW_EMU_XSA}" platform_out "${PLATFORM_DIR}" boot_dir_path "${BOOT_DIR}" img_dir_path "${IMAGE_DIR}"
if [ -d ${SW_DIR}/platform/filesystem ]; then 
	cp -rf ${SW_DIR}/platform/filesystem ${PLATFORM_DIR}/${PLATFORM_NAME}/export/${PLATFORM_NAME}/sw/${PLATFORM_NAME}/xrt/
fi
popd >> $REDIR_OUT
if [ -v _DEBUG ]; then set +x; fi
