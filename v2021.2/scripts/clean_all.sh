#!/bin/bash
_self=$(basename "$0")

# Bring in environment variables
. ../config/buildvars.sh
# Bring in petalinux configuration values
. ../config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}"
echo "################################################"

# Clean the hardware project
echo "################################################"
echo "# ${_self}: Cleaning hardware project"
echo "################################################"
pushd ${TOP_DIR}/hw
rm -r vivado* build .Xil *dynamic* *.log *.xpe 
popd

echo "################################################"
echo "# ${_self}: Cleaning software project"
echo "################################################"
# Build Petalinux
pushd ${TOP_DIR}/sw/petalinux
petalinux-build -x cleanall
popd

echo "################################################"
echo "# ${_self}: Cleaning platform files"
echo "################################################"
# Clean the Platform Files
pushd ${TOP_DIR}
rm -rf ${PLATFORM_DIR}
popd
