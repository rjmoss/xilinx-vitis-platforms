#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}/config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}: Showing SDK Installer"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
if [ -d ${SW_DIR}/deploy/sdk ]; then
	tree -L 1 ${SW_DIR}/deploy/sdk/
else
	echo "-- An SDK Installer was not found in ${SW_DIR}/deploy/sdk/"
	echo "-- See the top level Makefile for help building a software development SDK."
fi
if [ -v _DEBUG ]; then set +x; fi
