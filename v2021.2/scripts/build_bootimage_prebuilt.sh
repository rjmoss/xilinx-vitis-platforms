#!/bin/bash
_self=$(basename ${BASH_SOURCE[0]})

# Bring in environment variables
. ${TOP_DIR}/config/buildvars.sh
# Bring in petalinux configuration values
. ${TOP_DIR}/config/petalinuxconfigs.sh

echo "################################################"
echo "# ${_self}: Building boot image (prebuilt_linux)"
echo "################################################"

if [ -v _DEBUG ]; then set -x; fi
pushd ${TOP_DIR}/sw/prebuilt_linux >> $REDIR_OUT
if [ -f ${TOP_DIR}/sw/build/hw.bit ]; then
	${BOOTGEN} -arch zynqmp -image ./bootgen.bif -o ${BOOT_IMAGE} -w
else
	echo "WARNING: \"hw.bit\" file not found. Skipping BOOT image generation."
fi
popd >> $REDIR_OUT
if [ -v _DEBUG ]; then set +x; fi
