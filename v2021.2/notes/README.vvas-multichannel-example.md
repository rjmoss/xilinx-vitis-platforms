# Build the VVAS Multichanel Example for a Custom Platform

## References
- [VVAS v1.1 Release Source](https://github.com/Xilinx/VVAS/tree/VVAS_REL_v1.1)
- [VVAS Multichannel Example Source](https://github.com/Xilinx/VVAS/tree/VVAS_REL_v1.1/vvas-examples/Embedded/multichannel_ml)
- [VVAS Multichannel Example Project Instructions](https://xilinx.github.io/VVAS/main/build/html/docs/Embedded/Tutorials/MultiChannelML.html#vitis-example-project)


### Define the location of your custom Vitis Platform, Custom DPU
```bash
$ export CUSTOM_PFM=$HOME/repositories/gitlab/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_vcu_hdmitx_vai/platform_repo/xilinx_zcu106_vcu_hdmitx_vai_2021_2/export/xilinx_zcu106_vcu_hdmitx_vai_2021_2/xilinx_zcu106_vcu_hdmitx_vai_2021_2.xpfm

$ export CUSTOM_DPU_TRD=$HOME/repositories/gitlab/practical-introduction-to-vvas/examples/Vitis-AI/dsa/DPU-TRD

$ export CUSTOM_VVAS_REPO=$HOME/repositories/gitlab/practical-introduction-to-vvas/examples/VVAS/
```

### Modify the DPU project configuration
- Enable single DPU
- Adjust AXI Port assignments
- Note: This matches the DPU TRD build
- Edit ```config_file/prj_config```
```
[clock]

freqHz=300000000:DPUCZDX8G_1.aclk
freqHz=600000000:DPUCZDX8G_1.ap_clk_2

[connectivity]

sp=DPUCZDX8G_1.M_AXI_GP0:HPC1
sp=DPUCZDX8G_1.M_AXI_HP0:HP3
sp=DPUCZDX8G_1.M_AXI_HP2:HP3


[advanced]
misc=:solution_name=link

#param=compiler.addOutputTypes=sd_card
#param=compiler.skipTimingCheckAndFrequencyScaling=1

[vivado]
prop=run.impl_1.strategy=Performance_Explore
#param=place.runPartPlacer=0
```

- Edit ```dpu_conf.vh```
	- Change ````define URAM_DISABLE```` to ````define URAM_ENABLE````

### Modify the Makefile to make path adjustments
- Correct the ROOTFS_FILE configuration in the exported custom platform
- Old
```bash
ROOTFS_FILE = ${PLATFORM_DIR}/sw/*/filesystem/rootfs.ext4
```

- Corrected
```bash
ROOTFS_FILE = ${PLATFORM_DIR}/sw/*/xrt/filesystem/rootfs.ext4
```

### Build VVAS Example Project usng the Custom Vitis Platform and Custom DPU
- Change to the example directory in the VVAS repository
```bash
cd vvas-examples/Embedded/multichannel_ml
vvas-examples/Embedded/multichannel_ml$
```

- Execute the build
```bash
$ make PLATFORM=$CUSTOM_PFM DPU_TRD_PATH=$CUSTOM_DPU_TRD HW_ACCEL_PATH=$CUSTOM_VVAS_REPO/vvas-accel-hw/
```

- Review the built Multichannel ML example components
```bash
sd_card
├── arch.json
├── BOOT.BIN
├── boot.scr
├── dpu.xclbin
├── Image
├── system.dtb
└── xilinx_zcu106_vcu_hdmitx_vai.hwh

0 directories, 7 files
```
































- Checkout a a copy of the v1.1 release tag to a local development branch named ```working_copy```
```bash
$ cd VVAS
VVAS$ git tag --list
	VVAS_REL_v1.0
	VVAS_REL_v1.1

VVAS$ git checkout -b working_copy VVAS_REL_v1.1
```

- Source the custom platform SDK environment
```bash
VVAS$ source $CUSTOM_PFM_SDK/environment-setup-cortexa72-cortexa53-xilinx-linux
```

- Build VVAS for the custom target platform
```bash
VVAS$ ./build_install_vvas.sh Edge
```

- Review the built VVAS components
```bash
install
├── usr
│ ├── include
│ │ ├── gstreamer-1.0
│ │ │ └── gst
│ │ │     └── vvas
│ │ │         ├── gstinferenceclassification.h
│ │ │         ├── gstinferencemeta.h
│ │ │         ├── gstinferenceprediction.h
│ │ │         ├── gstvvasallocator.h
│ │ │         ├── gstvvasbufferpool.h
│ │ │         ├── gstvvascommon.h
│ │ │         ├── gstvvashdrmeta.h
│ │ │         ├── gstvvasinpinfer.h
│ │ │         └── gstvvasutils.h
│ │ └── vvas
│ │     ├── vvas_kernel.h
│ │     ├── vvaslogs.h
│ │     ├── vvasmeta.h
│ │     └── xrt_utils.h
│ └── lib
│     ├── gstreamer-1.0
│     │   ├── libgstvvas_xabrscaler.so
│     │   ├── libgstvvas_xfilter.so
│     │   ├── libgstvvas_xinfer.so
│     │   ├── libgstvvas_xmetaaffixer.so
│     │   ├── libgstvvas_xmultisrc.so
│     │   └── libgstvvas_xroigen.so
│     ├── libgstvvasallocator-1.0.so -> libgstvvasallocator-1.0.so.0
│     ├── libgstvvasallocator-1.0.so.0 -> libgstvvasallocator-1.0.so.0.1602.0
│     ├── libgstvvasallocator-1.0.so.0.1602.0
│     ├── libgstvvasbufferpool-1.0.so -> libgstvvasbufferpool-1.0.so.0
│     ├── libgstvvasbufferpool-1.0.so.0 -> libgstvvasbufferpool-1.0.so.0.1602.0
│     ├── libgstvvasbufferpool-1.0.so.0.1602.0
│     ├── libgstvvashdrmeta-1.0.so -> libgstvvashdrmeta-1.0.so.0
│     ├── libgstvvashdrmeta-1.0.so.0 -> libgstvvashdrmeta-1.0.so.0.1602.0
│     ├── libgstvvashdrmeta-1.0.so.0.1602.0
│     ├── libgstvvasinfermeta-1.0.so -> libgstvvasinfermeta-1.0.so.0
│     ├── libgstvvasinfermeta-1.0.so.0 -> libgstvvasinfermeta-1.0.so.0.1602.0
│     ├── libgstvvasinfermeta-1.0.so.0.1602.0
│     ├── libgstvvasinpinfermeta-1.0.so -> libgstvvasinpinfermeta-1.0.so.0
│     ├── libgstvvasinpinfermeta-1.0.so.0 -> libgstvvasinpinfermeta-1.0.so.0.1602.0
│     ├── libgstvvasinpinfermeta-1.0.so.0.1602.0
│     ├── libgstvvasutils.so -> libgstvvasutils.so.0
│     ├── libgstvvasutils.so.0 -> libgstvvasutils.so.0.1602.0
│     ├── libgstvvasutils.so.0.1602.0
│     ├── libvvasutil.so
│     ├── libvvas_xboundingbox.so
│     ├── libvvas_xdpuinfer.so
│     ├── libvvas_xpreprocessor.so
│     ├── libxrtutil.so
│     └── pkgconfig
│         ├── vvas-gst-plugins.pc
│         └── vvas-utils.pc
└── vvas_installer.tar.gz

9 directories, 45 files
```
