# Enable Vitis Video Analytics SDK 1.1 on Custom Platform with Vitis AI 2.0

## References
- [VVAS v1.1 Release Source](https://github.com/Xilinx/VVAS/tree/VVAS_REL_v1.1)

### Define the location of your custom platform SDK (sysroot)
```bash
$ export CUSTOM_PFM_SDK=$HOME/repositories/gitlab/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_vcu_hdmitx_vai/sw/petalinux/sysroot
```

### Build VVAS using the custom platform SDK
- Clone the VVAS repository and checkout the v1.1 release tag
```bash
$ git clone https://github.com/Xilinx/VVAS.git
```

- Checkout a a copy of the v1.1 release tag to a local development branch named ```working_copy```
```bash
$ cd VVAS
VVAS$ git tag --list
	VVAS_REL_v1.0
	VVAS_REL_v1.1

VVAS$ git checkout -b working_copy VVAS_REL_v1.1
```

- Source the custom platform SDK environment
```bash
VVAS$ source $CUSTOM_PFM_SDK/environment-setup-cortexa72-cortexa53-xilinx-linux
```

- Build VVAS for the custom target platform
```bash
VVAS$ ./build_install_vvas.sh Edge
```

- Review the built VVAS components
```bash
install
├── usr
│ ├── include
│ │ ├── gstreamer-1.0
│ │ │ └── gst
│ │ │     └── vvas
│ │ │         ├── gstinferenceclassification.h
│ │ │         ├── gstinferencemeta.h
│ │ │         ├── gstinferenceprediction.h
│ │ │         ├── gstvvasallocator.h
│ │ │         ├── gstvvasbufferpool.h
│ │ │         ├── gstvvascommon.h
│ │ │         ├── gstvvashdrmeta.h
│ │ │         ├── gstvvasinpinfer.h
│ │ │         └── gstvvasutils.h
│ │ └── vvas
│ │     ├── vvas_kernel.h
│ │     ├── vvaslogs.h
│ │     ├── vvasmeta.h
│ │     └── xrt_utils.h
│ └── lib
│     ├── gstreamer-1.0
│     │   ├── libgstvvas_xabrscaler.so
│     │   ├── libgstvvas_xfilter.so
│     │   ├── libgstvvas_xinfer.so
│     │   ├── libgstvvas_xmetaaffixer.so
│     │   ├── libgstvvas_xmultisrc.so
│     │   └── libgstvvas_xroigen.so
│     ├── libgstvvasallocator-1.0.so -> libgstvvasallocator-1.0.so.0
│     ├── libgstvvasallocator-1.0.so.0 -> libgstvvasallocator-1.0.so.0.1602.0
│     ├── libgstvvasallocator-1.0.so.0.1602.0
│     ├── libgstvvasbufferpool-1.0.so -> libgstvvasbufferpool-1.0.so.0
│     ├── libgstvvasbufferpool-1.0.so.0 -> libgstvvasbufferpool-1.0.so.0.1602.0
│     ├── libgstvvasbufferpool-1.0.so.0.1602.0
│     ├── libgstvvashdrmeta-1.0.so -> libgstvvashdrmeta-1.0.so.0
│     ├── libgstvvashdrmeta-1.0.so.0 -> libgstvvashdrmeta-1.0.so.0.1602.0
│     ├── libgstvvashdrmeta-1.0.so.0.1602.0
│     ├── libgstvvasinfermeta-1.0.so -> libgstvvasinfermeta-1.0.so.0
│     ├── libgstvvasinfermeta-1.0.so.0 -> libgstvvasinfermeta-1.0.so.0.1602.0
│     ├── libgstvvasinfermeta-1.0.so.0.1602.0
│     ├── libgstvvasinpinfermeta-1.0.so -> libgstvvasinpinfermeta-1.0.so.0
│     ├── libgstvvasinpinfermeta-1.0.so.0 -> libgstvvasinpinfermeta-1.0.so.0.1602.0
│     ├── libgstvvasinpinfermeta-1.0.so.0.1602.0
│     ├── libgstvvasutils.so -> libgstvvasutils.so.0
│     ├── libgstvvasutils.so.0 -> libgstvvasutils.so.0.1602.0
│     ├── libgstvvasutils.so.0.1602.0
│     ├── libvvasutil.so
│     ├── libvvas_xboundingbox.so
│     ├── libvvas_xdpuinfer.so
│     ├── libvvas_xpreprocessor.so
│     ├── libxrtutil.so
│     └── pkgconfig
│         ├── vvas-gst-plugins.pc
│         └── vvas-utils.pc
└── vvas_installer.tar.gz

9 directories, 45 files
```
