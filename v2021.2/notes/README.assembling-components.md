# Assemble Components into a Demo SD Card

## Components
- Custom ZCU106 Vitis Platform with Vitis AI 2.0 Support
- Custom DPU
- Custom VVAS Installation


### Custom Vitis Platform Output
```bash
images
├── boot
│   ├── bl31.elf
│   ├── BOOT.BIN
│   ├── fsbl.elf
│   ├── pmufw.elf
│   ├── system.dtb
│   └── u-boot.elf
├── filesystem
│   ├── rootfs.ext4
│   └── rootfs.tar.gz
└── image
    ├── boot.scr
    ├── Image
    └── system.dtb

3 directories, 11 files
```

### Custom DPU Output
```bash
sd_card/
├── arch.json
├── BOOT.BIN
├── boot.scr
├── dpu.xclbin
├── Image
├── system.dtb
└── xilinx_zcu106_vcu_hdmitx_vai.hwh

0 directories, 7 files
```

### Custom VVAS Output
```bash
install
├── usr
│   ├── include
│   └── lib
└── vvas_installer.tar.gz

3 directories, 1 file
```

### Custom VVAS Multichannel ML Example Output
```bash
sd_card
├── arch.json
├── BOOT.BIN
├── boot.scr
├── dpu.xclbin
├── Image
├── system.dtb
└── xilinx_zcu106_vcu_hdmitx_vai.hwh

0 directories, 7 files
```










