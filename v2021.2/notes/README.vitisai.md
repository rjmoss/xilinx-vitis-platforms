# Enable Vitis-AI 2.0 on MPSoC Custom Platform

## References
- How to use [recipes-vitis-ai](https://github.com/Xilinx/Vitis-AI/tree/master/tools/Vitis-AI-Recipes)

### Define the location of your Petalinux project
```bash
$ export PLNX_PROJECT_ROOT=$HOME/repositories/gitlab/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_vcu_hdmitx_vai/sw/petalinux
```

### Add Vitis AI 2.0 Bitbake Recipes to Petalinux Project
- Clone the Vitis AI Repository
```bash
$ git clone --recurse-submodules https://github.com/Xilinx/Vitis-AI 
```

- Copy the ```recipes-vitis-ai``` folder to the Petalinux project
```bash
$ cp -r Vitis-AI/tools/Vitis-AI-Recipes/recipes-vitis-ai $PLNX_PROJECT_ROOT/project-spec/meta-user
```

- Enable the Vitis AI libraries as rootfs configuration options (enables them in the rootfs configuration menus)
```bash
$ cat $PLNX_PROJECT_ROOT/project-spec/meta-user/conf/user-rootfsconfig
	...
	#Added for Vitis-AI
	CONFIG_vitis-ai-library
	CONFIG_vitis-ai-library-dev
	CONFIG_vitis-ai-library-dbg
```

- Add these packages to the active configuration
```bash
$ cd $PLNX_PROJECT_ROOT
$ petalinux-config -c rootfs
```

- Select:
```bash
Select user packages --->
Select [*] vitis-ai-library
```

- Verify Vitis-AI components are enabled
```bash
$ cat $PLNX_PROJECT_ROOT/project-spec/configs/rootfs_config
	...
	#
	# user packages 
	#
	...
	CONFIG_vitis-ai-library=y
	CONFIG_vitis-ai-library-dbg=y
	CONFIG_vitis-ai-library-dev=y
```
