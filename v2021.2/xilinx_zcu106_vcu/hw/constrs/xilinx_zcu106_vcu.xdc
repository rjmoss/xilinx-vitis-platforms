#(C) Copyright 2020 - 2021 Xilinx, Inc.
#SPDX-License-Identifier: Apache-2.0
#-------------------------------------------------------------------

#####
## Constraints for ZCU106
## Version 1.0
#####


#####
## Pins
#####
set_property PACKAGE_PIN AH12 [get_ports si570_user_clk_p]
set_property IOSTANDARD LVDS [get_ports si570_user_clk_p]

set_clock_groups -name mux_clk -logically_exclusive -group [get_clocks [list  [get_clocks -of_objects [get_pins bd_i/clk_wiz_1/inst/mmcme4_adv_inst/CLKOUT1]]]] -group [get_clocks clk_pl_2]
