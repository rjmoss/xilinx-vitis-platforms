# Copyright 2021 Xilinx Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Makefile to pass arguments to TCL script:
# argv[0] : PLATFORM
# argv[1] : PLATFORM_NAME
# argv[2] : BOARD_NAME
# argv[3] : VER
# argv[4] : EMU_SUPPORT
# argv[5] : PRE_SYNTH


if { $argc < 5 } {
  puts "Error: TCL scripts require at least 4 arguments"
  exit
} else {
  # Direct arguments
  set PLATFORM [lindex $argv 0]
  set PLATFORM_NAME [lindex $argv 1]
  set BOARD_NAME [lindex $argv 2]
  set VER [lindex $argv 3]
  set EMU_SUPPORT [lindex $argv 4]
  # Derive platform name
  #set PLATFORM_NAME ${PLATFORM}_${VER}
  if { $argc == 6 } {
    set PRE_SYNTH [lindex $argv 5]
  } else {
    set PRE_SYNTH ""
  }
}

# Paths
set IP_DIR "ip"
set CONSTRS_DIR "constrs"
set BUILD_DIR "build"

# Display script arguments and other configuration values
puts "TCL SCRIPT ARGUMENTS:"
puts "---------------------"
puts "PLATFORM      = $PLATFORM"
puts "PLATFORM_NAME = $PLATFORM_NAME"
puts "BOARD_NAME    = $BOARD_NAME"
puts "VER           = $VER"
puts "PRE_SYNTH     = $PRE_SYNTH"
puts "EMU_SUPPORT   = $EMU_SUPPORT"
puts "---------------------"
puts "PATHS:"
puts "---------------------"
puts "BUILD_DIR     = $BUILD_DIR"
puts "CONSTRS_DIR   = $CONSTRS_DIR"
puts "IP_DIR        = $IP_DIR"
puts "---------------------"

