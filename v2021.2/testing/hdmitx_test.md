# Generate a software test pattern and send to the HDMI Display

## Identify the HDMI Display (Video Mixer) output device
```bash
root@xilinx_zcu106_vcu_hdmitx-2021:~# dmesg | grep v_mix
	...
	[    7.578779] xlnx-mixer b0140000.v_mix: Registered mixer CRTC with id: 43
	[    7.594951] xlnx-drm xlnx-drm.0: bound b0140000.v_mix (ops 0xffff800010e7f820)
	[    7.805945] xlnx-mixer b0140000.v_mix: [drm] fb0: xlnxdrmfb frame buffer device
	[    7.830127] [drm] Initialized xlnx 1.0.0 20130509 for b0140000.v_mix on minor 2
	...
```

## List valid display modes
```bash
root@xilinx_zcu106_vcu_hdmitx-2021:~# modetest -D b0140000.v_mix
	trying to open device 'i915'...done
	Encoders:
	id      crtc    type    possible crtcs  possible clones
	44      0       TMDS    0x00000001      0x00000001

	Connectors:
	id      encoder status          name            size (mm)       modes   encoders
	45      0       connected       HDMI-A-1        630x360         35      44
	  modes:
	        index name refresh (Hz) hdisp hss hse htot vdisp vss vse vtot)
	  #0 3840x2160 60.00 3840 4016 4104 4400 2160 2168 2178 2250 594000 flags: phsync, pvsync; type: preferred, driver
	  #1 3840x2160 59.94 3840 4016 4104 4400 2160 2168 2178 2250 593407 flags: phsync, pvsync; type: driver
	  #2 3840x2160 50.00 3840 4896 4984 5280 2160 2168 2178 2250 594000 flags: phsync, pvsync; type: driver
	  #3 3840x2160 30.00 3840 4016 4104 4400 2160 2168 2178 2250 297000 flags: phsync, pvsync; type: driver
	  #4 3840x2160 29.97 3840 4016 4104 4400 2160 2168 2178 2250 296703 flags: phsync, pvsync; type: driver
	  #5 1920x1080 60.00 1920 2008 2052 2200 1080 1084 1089 1125 148500 flags: phsync, pvsync; type: driver
	  #6 1920x1080 59.94 1920 2008 2052 2200 1080 1084 1089 1125 148352 flags: phsync, pvsync; type: driver
	  #7 1920x1080 50.00 1920 2448 2492 2640 1080 1084 1089 1125 148500 flags: phsync, pvsync; type: driver
	  #8 1680x1050 59.88 1680 1728 1760 1840 1050 1053 1059 1080 119000 flags: phsync, nvsync; type: driver
	  #9 1600x900 60.00 1600 1624 1704 1800 900 901 904 1000 108000 flags: phsync, pvsync; type: driver
	  #10 1280x1024 75.02 1280 1296 1440 1688 1024 1025 1028 1066 135000 flags: phsync, pvsync; type: driver
	  #11 1280x1024 60.02 1280 1328 1440 1688 1024 1025 1028 1066 108000 flags: phsync, pvsync; type: driver
	  #12 1440x900 59.90 1440 1488 1520 1600 900 903 909 926 88750 flags: phsync, nvsync; type: driver
	  #13 1280x800 59.91 1280 1328 1360 1440 800 803 809 823 71000 flags: phsync, nvsync; type: driver
	  #14 1152x864 75.00 1152 1216 1344 1600 864 865 868 900 108000 flags: phsync, pvsync; type: driver
	  #15 1280x720 60.00 1280 1390 1430 1650 720 725 730 750 74250 flags: phsync, pvsync; type: driver
	  #16 1280x720 59.94 1280 1390 1430 1650 720 725 730 750 74176 flags: phsync, pvsync; type: driver
	  #17 1280x720 50.00 1280 1720 1760 1980 720 725 730 750 74250 flags: phsync, pvsync; type: driver
	  #18 1024x768 75.03 1024 1040 1136 1312 768 769 772 800 78750 flags: phsync, pvsync; type: driver
	  #19 1024x768 70.07 1024 1048 1184 1328 768 771 777 806 75000 flags: nhsync, nvsync; type: driver
	  #20 1024x768 60.00 1024 1048 1184 1344 768 771 777 806 65000 flags: nhsync, nvsync; type: driver
	  #21 832x624 74.55 832 864 928 1152 624 625 628 667 57284 flags: nhsync, nvsync; type: driver
	  #22 800x600 75.00 800 816 896 1056 600 601 604 625 49500 flags: phsync, pvsync; type: driver
	  #23 800x600 72.19 800 856 976 1040 600 637 643 666 50000 flags: phsync, pvsync; type: driver
	  #24 800x600 60.32 800 840 968 1056 600 601 605 628 40000 flags: phsync, pvsync; type: driver
	  #25 800x600 56.25 800 824 896 1024 600 601 603 625 36000 flags: phsync, pvsync; type: driver
	  #26 720x576 50.00 720 732 796 864 576 581 586 625 27000 flags: nhsync, nvsync; type: driver
	  #27 720x480 60.00 720 736 798 858 480 489 495 525 27027 flags: nhsync, nvsync; type: driver
	  #28 720x480 59.94 720 736 798 858 480 489 495 525 27000 flags: nhsync, nvsync; type: driver
	  #29 640x480 75.00 640 656 720 840 480 481 484 500 31500 flags: nhsync, nvsync; type: driver
	  #30 640x480 72.81 640 664 704 832 480 489 492 520 31500 flags: nhsync, nvsync; type: driver
	  #31 640x480 66.67 640 704 768 864 480 483 486 525 30240 flags: nhsync, nvsync; type: driver
	  #32 640x480 60.00 640 656 752 800 480 490 492 525 25200 flags: nhsync, nvsync; type: driver
	  #33 640x480 59.94 640 656 752 800 480 490 492 525 25175 flags: nhsync, nvsync; type: driver
	  #34 720x400 70.08 720 738 846 900 400 412 414 449 28320 flags: nhsync, pvsync; type: driver
	  props:
	        1 EDID:
	                flags: immutable blob
	                blobs:

	                value:
	                        00ffffffffffff004c2d171055373930
	                        181f0103803f24782ac8b5ad50449e25
	                        0f5054bfef80714f810081c081809500
	                        a9c0b300010108e80030f2705a80b058
	                        8a0078682100001e000000fd00324b1e
	                        873c000a202020202020000000fc004c
	                        5532385235350a2020202020000000ff
	                        0048434a523630393133380a202001ce
	                        020335f04961120313041f10605f2309
	                        070783010000e305c0006b030c001200
	                        b83c2000200167d85dc401788003e20f
	                        81e3060501023a801871382d40582c45
	                        0078682100001e023a80d072382d4010
	                        2c458078682100001e00000000000000
	                        00000000000000000000000000000000
	                        00000000000000000000000000000099
	        2 DPMS:
	                flags: enum
	                enums: On=0 Standby=1 Suspend=2 Off=3
	                value: 0
	        5 link-status:
	                flags: enum
	                enums: Good=0 Bad=1
	                value: 0
	        6 non-desktop:
	                flags: immutable range
	                values: 0 1
	                value: 0
	        4 TILE:
	                flags: immutable blob
	                blobs:

	                value:
	        8 GEN_HDR_OUTPUT_METADATA:
	                flags: blob
	                blobs:

	                value:
	        46 colorspace:
	                flags: range
	                values: 0 12
	                value: 0
	        47 ycbcr_enc:
	                flags: range
	                values: 0 8
	                value: 0
	        48 xfer_func:
	                flags: range
	                values: 0 7
	                value: 0
	        49 quantization:
	                flags: range
	                values: 0 2
	                value: 0

	CRTCs:
	id      fb      pos     size
	43      0       (0,0)   (0x0)
	  #0  nan 0 0 0 0 0 0 0 0 0 flags: ; type: 
	  props:
	        25 VRR_ENABLED:
	                flags: range
	                values: 0 1
	                value: 0

	Planes:
	id      crtc    fb      CRTC x,y        x,y     gamma size      possible crtcs
	34      0       0       0,0             0,0     0               0x00000001
	  formats: NV12
	  props:
	        9 type:
	                flags: immutable enum
	                enums: Overlay=0 Primary=1 Cursor=2
	                value: 0
	35      0       0       0,0             0,0     0               0x00000001
	  formats: NV12
	  props:
	        9 type:
	                flags: immutable enum
	                enums: Overlay=0 Primary=1 Cursor=2
	                value: 0
	36      0       0       0,0             0,0     0               0x00000001
	  formats: NV12
	  props:
	        9 type:
	                flags: immutable enum
	                enums: Overlay=0 Primary=1 Cursor=2
	                value: 0
	37      0       0       0,0             0,0     0               0x00000001
	  formats: NV12
	  props:
	        9 type:
	                flags: immutable enum
	                enums: Overlay=0 Primary=1 Cursor=2
	                value: 0
	38      0       0       0,0             0,0     0               0x00000001
	  formats: NV12
	  props:
	        9 type:
	                flags: immutable enum
	                enums: Overlay=0 Primary=1 Cursor=2
	                value: 0
	39      0       0       0,0             0,0     0               0x00000001
	  formats: NV12
	  props:
	        9 type:
	                flags: immutable enum
	                enums: Overlay=0 Primary=1 Cursor=2
	                value: 0
	40      0       0       0,0             0,0     0               0x00000001
	  formats: NV12
	  props:
	        9 type:
	                flags: immutable enum
	                enums: Overlay=0 Primary=1 Cursor=2
	                value: 0
	41      0       0       0,0             0,0     0               0x00000001
	  formats: NV12
	  props:
	        9 type:
	                flags: immutable enum
	                enums: Overlay=0 Primary=1 Cursor=2
	                value: 0
	42      0       0       0,0             0,0     0               0x00000001
	  formats: BG24
	  props:
	        9 type:
	                flags: immutable enum
	                enums: Overlay=0 Primary=1 Cursor=2
	                value: 1

	Frame buffers:
	id      size    pitch
```

## List the video test pattern options
- use the ```videotestsrc``` gstreamer element
```bash
root@xilinx_zcu106_vcu_hdmitx-2021:~# gst-inspect-1.0 videotestsrc
	Factory Details:
	  Rank                     none (0)
	  Long-name                Video test source
	  Klass                    Source/Video
	  Description              Creates a test video stream
	  Author                   David A. Schleef <ds@schleef.org>

	Plugin Details:
	  Name                     videotestsrc
	...
	Element Properties:
	...
	  pattern             : Type of test pattern to generate
	                        flags: readable, writable
	                        Enum "GstVideoTestSrcPattern" Default: 0, "smpte"
	                           (0): smpte            - SMPTE 100% color bars
	                           (1): snow             - Random (television snow)
	                           (2): black            - 100% Black
	                           (3): white            - 100% White
	                           (4): red              - Red
	                           (5): green            - Green
	                           (6): blue             - Blue
	                           (7): checkers-1       - Checkers 1px
	                           (8): checkers-2       - Checkers 2px
	                           (9): checkers-4       - Checkers 4px
	                           (10): checkers-8       - Checkers 8px
	                           (11): circular         - Circular
	                           (12): blink            - Blink
	                           (13): smpte75          - SMPTE 75% color bars
	                           (14): zone-plate       - Zone plate
	                           (15): gamut            - Gamut checkers
	                           (16): chroma-zone-plate - Chroma zone plate
	                           (17): solid-color      - Solid color
	                           (18): ball             - Moving ball
	                           (19): smpte100         - SMPTE 100% color bars
	                           (20): bar              - Bar
	                           (21): pinwheel         - Pinwheel
	                           (22): spokes           - Spokes
	                           (23): gradient         - Gradient
	                           (24): colors           - Colors
	...
```

## Display a test pattern in full screen
- 640x480 with forced mode change
```bash
gst-launch-1.0 -v videotestsrc pattern=0 ! \
video/x-raw,width=640,height=480,framerate=30/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" \
fullscreen-overlay=true
```

- 1024x768 with forced mode change
```bash
gst-launch-1.0 -v videotestsrc pattern=0 ! \
video/x-raw,width=1024,height=768,framerate=15/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" \
fullscreen-overlay=true
```

- 1920x1080 with forced mode change
```bash
gst-launch-1.0 -v videotestsrc pattern=0 ! \
video/x-raw,width=1920,height=1080,framerate=10/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" \
fullscreen-overlay=true
```

- 3840x2160 with forced mode change
```bash
gst-launch-1.0 -v videotestsrc pattern=0 ! \
video/x-raw,width=3840,height=2160,framerate=3/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" \
fullscreen-overlay=true
```