# Decode and playback compressed video to the HDMI Display

## Download Test Videos
- Get the Xilinx Big Buck Bunny test clip
- Store on rootfs (SD Card)
```bash
root@xilinx_zcu106_vcu_hdmitx-2021:~# wget petalinux.xilinx.com/sswreleases/video-files/bbb_sunflower_2160p_30fps_normal_avc.mp4
	Connecting to petalinux.xilinx.com (96.17.76.144:80)
	saving to 'bbb_sunflower_2160p_30fps_normal_avc.mp4'
	bbb_sunflower_2160p_ 100% |*****************************************************************************************************************************************************************************************************************************************| 94.7M  0:00:00 ETA
	'bbb_sunflower_2160p_30fps_normal_avc.mp4' saved
```

## Analyze the video file
```bash
root@xilinx_zcu106_vcu_hdmitx-2021:~# gst-discoverer-1.0 bbb_sunflower_2160p_30fps_normal_avc.mp4 -v
	Analyzing file:///home/root/bbb_sunflower_2160p_30fps_normal_avc.mp4
	Done discovering file:///home/root/bbb_sunflower_2160p_30fps_normal_avc.mp4

	Topology:
	  container: video/quicktime, variant=(string)iso
	    audio: audio/mpeg, mpegversion=(int)4, framed=(boolean)true, stream-format=(string)raw, level=(string)4, base-profile=(string)lc, profile=(string)lc, codec_data=(buffer)11b056e500,6
	      Tags:
	        audio codec: MPEG-4 AAC audio
	        maximum bitrate: 341266
	        bitrate: 341266
	        encoder: Lavf57.63.100
	        container format: ISO MP4/M4A
	      
	      Codec:
	        audio/mpeg, mpegversion=(int)4, framed=(boolean)true, stream-format=(string)raw, level=(string)4, base-profile=(string)lc, profile=(string)lc, codec_data=(buffer)11b056e500, ra6
	      Additional info:
	        None
	      Stream ID: ee0d650e89af455db1b3dd6a170d298c3664bec42ea342224a7daa29aba05ed2/002
	      Language: <unknown>
	      Channels: 6 (front-left, front-right, front-center, lfe1, rear-left, rear-right)
	      Sample rate: 48000
	      Depth: 16
	      Bitrate: 341266
	      Max bitrate: 341266
	    video: video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)5.1, profile=(string)high, width=(int)3840, height=(int)2160, framerate=(fraction)123/4,e
	      Tags:
	        video codec: H.264 / AVC
	        bitrate: 6271384
	        encoder: Lavf57.63.100
	        container format: ISO MP4/M4A
	      
	      Codec:
	        video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)5.1, profile=(string)high, width=(int)3840, height=(int)2160, framerate=(fraction)123/4, pie
	      Additional info:
	        None
	      Stream ID: ee0d650e89af455db1b3dd6a170d298c3664bec42ea342224a7daa29aba05ed2/001
	      Width: 3840
	      Height: 2160
	      Depth: 24
	      Frame rate: 123/4
	      Pixel aspect ratio: 1/1
	      Interlaced: false
	      Bitrate: 6271384
	      Max bitrate: 0

	Properties:
	  Duration: 0:02:00.026000000
	  Seekable: yes
	  Live: no
	  Tags: 
	      audio codec: MPEG-4 AAC audio
	      maximum bitrate: 341266
	      bitrate: 341266
	      encoder: Lavf57.63.100
	      container format: ISO MP4/M4A
	      video codec: H.264 / AVC
```

- Note the framerate is listed as ```123/4```
```bash
gst-launch-1.0 -v filesrc location=/home/root/bbb_sunflower_2160p_30fps_normal_avc.mp4 ! \
qtdemux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! video/x-raw,format=NV12,width=3840,height=2160,framerate=123/4 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" \
fullscreen-overlay=true
```
