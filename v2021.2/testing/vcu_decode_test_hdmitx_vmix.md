# Run multiple pipelines at different resolutiosn using video mixer planes with the HDMI Display

## Download Test Videos
- Get video test clips and store on rootfs (SD Card)

### Ultra Video Group Bosphorous test clip (1920x1080)
```bash
root@xilinx_zcu106_vcu_hdmitx-2021:~# wget http://ultravideo.cs.tut.fi/video/Bosphorus_1920x1080_30fps_420_8bit_AVC_MP4.mp4
	Connecting to ultravideo.cs.tut.fi (130.230.88.78:80)
	Connecting to ultravideo.fi (130.230.88.78:80)
	saving to 'Bosphorus_1920x1080_30fps_420_8bit_AVC_MP4.mp4'
	Bosphorus_1920x1080_ 100% |*****************************************************************************************************************************************************************************************************************************************| 7332k  0:00:00 ETA
	'Bosphorus_1920x1080_30fps_420_8bit_AVC_MP4.mp4' saved
```

### Blender Big Buck Bunny test clip (1920x1080)
```bash
root@xilinx_zcu106_vcu_hdmitx-2021:~#  wget http://ftp.vim.org/ftp/ftp/pub/graphics/blender/demo/movies/BBB/bbb_sunflower_1080p_30fps_normal.mp4
	Connecting to ftp.vim.org (145.220.21.40:80)
	saving to 'bbb_sunflower_1080p_30fps_normal.mp4'
	bbb_sunflower_1080p_ 100% |*****************************************************************************************************************************************************************************************************************************************|  263M  0:00:00 ETA
	'bbb_sunflower_1080p_30fps_normal.mp4' saved
```

### Caminandes Llama Amigos test clip (1920x1080)
```bash
wget http://www.caminandes.com/download/03_caminandes_llamigos_1080p.mp4
	Connecting to www.caminandes.com (208.113.198.74:80)
	saving to '03_caminandes_llamigos_1080p.mp4'
	03_caminandes_llamig 100% |*****************************************************************************************************************************************************************************************************************************************|  191M  0:00:00 ETA
	'03_caminandes_llamigos_1080p.mp4' saved
```

### Sintel Trailer test clip (1280x720)
```bash
root@xilinx_zcu106_vcu_hdmitx-2021:~# wget https://download.blender.org/durian/trailer/sintel_trailer-720p.mp4
	Connecting to download.blender.org (82.94.213.221:443)
	wget: note: TLS certificate validation not implemented
	saving to 'sintel_trailer-720p.mp4'
	sintel_trailer-720p. 100% |*****************************************************************************************************************************************************************************************************************************************| 7429k  0:00:00 ETA
	'sintel_trailer-720p.mp4' saved
```

### Philips Air Show test clipe (1920x1080)
```bash
root@xilinx_zcu106_vcu_hdmitx-2021:~# wget https://archive.org/download/dweu-philips/philips_air_show-DWEU.mkv
	Connecting to archive.org (207.241.224.2:443)
	wget: note: TLS certificate validation not implemented
	Connecting to ia802907.us.archive.org (207.241.233.57:443)
	saving to 'philips_air_show-DWEU.mkv'
	philips_air_show-DWE 100% |*****************************************************************************************************************************************************************************************************************************************|  234M  0:00:00 ETA
	'philips_air_show-DWEU.mkv' saved
```

## Plan the video mixer plane use (for video playback)
- Video Mixer plane stack and display priority
- Higher plane numbers are on "top" (closer to the foreground)
```bash
|---------------------------------------
| Plane #42
| |-------------------------------------
| | Plane #34
| | |-----------------------------------
| | | Plane #35
| | | |---------------------------------
| | | | Plane #36
| | | | |-------------------------------
| | | | | Plane #37
| | | | | |-----------------------------
| | | | | | | Plane #38
| | | | | | | |-------------------------
| | | | | | | | Plane #39
| | | | | | | | |-----------------------
| | | | | | | | | Plane #40
| | | | | | | | | ----------------------
| | | | | | | | | | Plane #41
...
| | | | | | | | | |---------------------
| | | | | | | | |-----------------------
| | | | | | | |-------------------------
| | | | | |-----------------------------
| | | | |-------------------------------
| | | |---------------------------------
| | |-----------------------------------
| |-------------------------------------
|---------------------------------------
```

- Illustrate plane ordering with test pattern generation
- Bouncing Ball Full screen in background + test pattern centered in foreground
```bash
gst-launch-1.0 -v videotestsrc pattern=18 ! \
video/x-raw,width=3840,height=2160,framerate=1/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=34 & \
\
\
gst-launch-1.0 -v videotestsrc pattern=0 ! \
video/x-raw,width=640,height=480,framerate=5/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=35 render-rectangle="<1600,840,640,480>" &
```

- Layout playback to highlight plane priorities
```
########################################################################
# HDMI-TX Video Mixer Framebuffer Configuration
########################################################################
# CONNECTOR:    45
# CRTC:         43
# PLANE:        42 (Primary, Frame Buffer Output) BG24
# PLANE:        34-41 (Primary, Frame Buffer) NV12
########################################################################
# Display Layout #1 - 4 Quadrants
# ----------------------------
# - Native Resolution: 3840x2160@24fps (to match video content)
# ------------------------------
#
# 	 [0,0]|--------------[1920,0   ]--------------|[3840,0]
#         |                   |   |-----------|   |
#         |                   |   |           |   |
#         |  FILE #1          |   |  File #2  |   |
#         |  1080p            |   |  720p     |   |
#         |                   |   |-----------|   |
# [0,1080]|--------------[1920,1080]--------------|[3840,1080]
#         |                   |                   |
#         |                   |                   |
#         |   File #3         |       FILE #4     |
#         |   1080p           |       1080p       |
#         |                   |                   |
# [0,2160]|--------------[1920,2160]--------------|[3840,2160]
#
########################################################################
# Display Layout #2 - 4 Quadrants with overlap
# ----------------------------
# - Native Resolution: 3840x2160@24fps (to match video content)
# ------------------------------
#
# 	 [0,0]|--------------[1920,0   ]--------------|[3840,0]
#         |                   |   |-----------|   |
#         |                   |   |           |   |
#         |                   |   |  File #2  |   |
#         |         |-----------------|720p   |   |
#         |         |     FILE #1     |-------|   |
# [0,1080]|---------|     1080p       |-----------|[3840,1080]
#         |         |                 |           |
#         |         |                 |           |
#         |         |-----------------|File #4    |
#         |  File #3           |       1080p      |
#         |  1080p             |                  |
# [0,2160]|--------------[1920,2160]--------------|[3840,2160]
#
########################################################################
########################################################################
########################################################################
```

## Analyze the video files and identify the GStreamer Decode Pipelines

### File #1
- Ultra Video Group Bosphorous test clip (1920x1080)
- Analyze the video file (abbreviated output below)
```bash
root@xilinx_zcu106_vcu_hdmitx-2021:~# gst-discoverer-1.0 Bosphorus_1920x1080_30fps_420_8bit_AVC_MP4.mp4 -v
	...
	Topology:
	  container: video/quicktime, variant=(string)iso
	    video: video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)4, profile=(string)high, width=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:0,e
	...
```

- Representative pipeline with important parameters highlighted
```bash
Pipeline Elements:
------------------
[FILESRC] -> [QTDEMUX] -> [H264PARSE] -> [OMXH264DEC] -> [QUEUE] -> [KMSSINK]

[OMXH264DEC] Parameters:
------------------------
video/x-raw,format=NV12,width=1920,height=1080,framerate=30/1

[KMSSINK] Parameters (Layout #1):
---------------------------------
bus-id="b0140000.v_mix"
render-rectangle="<0,0,1920,1080>"
plane-id=34

[KMSSINK] Parameters (Layout #2):
---------------------------------
bus-id="b0140000.v_mix"
render-rectangle="<960,540,1920,1080>"
plane-id=34
```

### File #1
- Blender Big Buck Bunny test video (1920x1080)
- Analyze the video file (abbreviated output below)
```bash
root@xilinx_zcu106_vcu_hdmitx-2021:~# gst-discoverer-1.0 bbb_sunflower_1080p_30fps_normal.mp4 -v
	...
	Topology:
	  container: video/quicktime, variant=(string)iso
	    video: video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)4.1, profile=(string)high, width=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:e
	...
```

- Representative pipeline with important parameters highlighted
```bash
Pipeline Elements:
------------------
[FILESRC] -> [QTDEMUX] -> [H264PARSE] -> [OMXH264DEC] -> [QUEUE] -> [KMSSINK]

[OMXH264DEC] Parameters:
------------------------
video/x-raw,format=NV12,width=1920,height=1080,framerate=30/1

[KMSSINK] Parameters (Layout #1):
---------------------------------
bus-id="b0140000.v_mix"
render-rectangle="<0,0,1920,1080>"
plane-id=34

[KMSSINK] Parameters (Layout #2):
---------------------------------
bus-id="b0140000.v_mix"
render-rectangle="<960,540,1920,1080>"
plane-id=34
```

### File #2
- Sintel Trailer test clip (1280x720)
- Analyze the video file (abbreviated output below)
```bash
root@xilinx_zcu106_vcu_hdmitx-2021:~# gst-discoverer-1.0 sintel_trailer-720p.mp4  -v
	...
	Topology:
  	container: video/quicktime, variant=(string)iso
	    video: video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)3.1, profile=(string)high, width=(int)1280, height=(int)720, framerate=(fraction)24/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:0e
	...
```

- Representative pipeline with important parameters highlighted
```bash
Pipeline Elements:
------------------
[FILESRC] -> [QTDEMUX] -> [H264PARSE] -> [OMXH264DEC] -> [QUEUE] -> [KMSSINK]

[OMXH264DEC] Parameters:
------------------------
video/x-raw,format=NV12,width=1280,height=720,framerate=24/1

[KMSSINK] Parameters (Layout #1):
---------------------------------
bus-id="b0140000.v_mix"
render-rectangle="<2240,180,1280,720>"
plane-id=35

[KMSSINK] Parameters (Layout #2):
---------------------------------
bus-id="b0140000.v_mix"
render-rectangle="<2240,180,1280,720>"
plane-id=35
```

### File #3
- Philips Air Show test clipe (1920x1080)
- Analyze the video file (abbreviated output below)
```bash
root@xilinx_zcu106_vcu_hdmitx-2021:~# gst-discoverer-1.0 philips_air_show-DWEU.mkv -v
	...
	Topology:
	  container: video/x-matroska
	    video: video/x-h264, level=(string)4.1, profile=(string)high, stream-format=(string)byte-stream, alignment=(string)au, width=(int)1920, height=(int)1080, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)30000/1001, interlace-mode=(string)progressive, chroma-format=(strine
	...
```

- Representative pipeline with important parameters highlighted
```bash
Pipeline Elements:
------------------
[FILESRC] -> [MATROSKADEMUX] -> [H264PARSE] -> [OMXH264DEC] -> [QUEUE] -> [KMSSINK]

[OMXH264DEC] Parameters:
------------------------
video/x-raw,format=NV12,width=1920,height=1080,framerate=30000/1001

[KMSSINK] Parameters (Layout #1):
---------------------------------
bus-id="b0140000.v_mix"
render-rectangle="<0,1080,1920,1080>"
plane-id=36

[KMSSINK] Parameters (Layout #2):
---------------------------------
bus-id="b0140000.v_mix"
render-rectangle="<0,1080,1920,1080>"
plane-id=36
```

### File #4
- Caminandes Llama Amigos test clip (1920x1080)
```bash
root@xilinx_zcu106_vcu_hdmitx-2021:~# gst-discoverer-1.0 03_caminandes_llamigos_1080p.mp4 -v
	...
	Topology:
	  container: video/quicktime, variant=(string)iso
		...
		video: video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)5, profile=(string)high, width=(int)1920, height=(int)1080, framerate=(fraction)24/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:0,e
	...
```

- Representative pipeline with important parameters highlighted
```bash
Pipeline Elements:
------------------
[FILESRC] -> [QTDEMUX] -> [H264PARSE] -> [OMXH264DEC] -> [QUEUE] -> [KMSSINK]

[OMXH264DEC] Parameters:
------------------------
video/x-raw,format=NV12,width=1920,height=1080,framerate=30/1

[KMSSINK] Parameters (Layout #1):
---------------------------------
bus-id="b0140000.v_mix"
render-rectangle="<1920,1080,1920,1080>"
plane-id=37

[KMSSINK] Parameters (Layout #2):
---------------------------------
bus-id="b0140000.v_mix"
render-rectangle="<1920,1080,1920,1080>"
plane-id=37
```

## Video Mixer / HDMI Display Primary Plane Setup
- Set to 3840x2160@60Hz, with SMPTE colorbar test pattern
- Syntax: ```-s <connector>@<crtc>:<width>x<height>-<framerate>@<colorformat>```
- Optional: ```-F <fill pattern>```
	- ```tiles```, ```smpte```, ```plain```, ```gradient```

```bash
modetest -M xlnx -D b0140000.v_mix -s 45@43:3840x2160-60@BG24 &
```

## GStreamer Video Decode Pipelines

### Layout #1

#### Individual Pipelines
- File #1
```bash
gst-launch-1.0 -v filesrc location=/home/root/\
Bosphorus_1920x1080_30fps_420_8bit_AVC_MP4.mp4 ! \
qtdemux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! \
video/x-raw,format=NV12,width=1920,height=1080,framerate=30/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=34 render-rectangle="<0,0,1920,1080>"
```

- File #2
```bash
gst-launch-1.0 -v filesrc location=/home/root/\
sintel_trailer-720p.mp4 ! \
qtdemux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! \
video/x-raw,format=NV12,width=1280,height=720,framerate=24/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=35 render-rectangle="<2240,180,1280,720>"
```

- File #3
```bash
gst-launch-1.0 -v filesrc location=/home/root/\
philips_air_show-DWEU.mkv ! \
matroskademux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! \
video/x-raw,format=NV12,width=1920,height=1080,framerate=30000/1001 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=36 render-rectangle="<0,1080,1920,1080>"
```

- File #4
```bash
gst-launch-1.0 -v filesrc location=/home/root/\
03_caminandes_llamigos_1080p.mp4 ! \
qtdemux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! \
video/x-raw,format=NV12,width=1920,height=1080,framerate=24/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=37 render-rectangle="<1920,1080,1920,1080>"
```

#### Combined Pipeline
```bash
gst-launch-1.0 -v filesrc location=/home/root/\
Bosphorus_1920x1080_30fps_420_8bit_AVC_MP4.mp4 ! \
qtdemux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! \
video/x-raw,format=NV12,width=1920,height=1080,framerate=30/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=34 render-rectangle="<0,0,1920,1080>" & \
\
\
gst-launch-1.0 -v filesrc location=/home/root/\
sintel_trailer-720p.mp4 ! \
qtdemux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! \
video/x-raw,format=NV12,width=1280,height=720,framerate=24/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=35 render-rectangle="<2240,180,1280,720>" & \
\
\
gst-launch-1.0 -v filesrc location=/home/root/\
philips_air_show-DWEU.mkv ! \
matroskademux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! \
video/x-raw,format=NV12,width=1920,height=1080,framerate=30000/1001 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=36 render-rectangle="<0,1080,1920,1080>" & \
\
\
gst-launch-1.0 -v filesrc location=/home/root/\
03_caminandes_llamigos_1080p.mp4 ! \
qtdemux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! \
video/x-raw,format=NV12,width=1920,height=1080,framerate=24/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=37 render-rectangle="<1920,1080,1920,1080>" &
```

- Stop playback pipelines by killing all background processes in the shell
```bash
kill $(jobs -p)
```

### Layout #2

#### Individual Pipelines
- File #1
```bash
gst-launch-1.0 -v filesrc location=/home/root/\
Bosphorus_1920x1080_30fps_420_8bit_AVC_MP4.mp4 ! \
qtdemux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! \
video/x-raw,format=NV12,width=1920,height=1080,framerate=30/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=34 render-rectangle="<960,540,1920,1080>"
```

- File #2
```bash
gst-launch-1.0 -v filesrc location=/home/root/\
sintel_trailer-720p.mp4 ! \
qtdemux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! \
video/x-raw,format=NV12,width=1280,height=720,framerate=24/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=35 render-rectangle="<2240,180,1280,720>"
```

- File #3
```bash
gst-launch-1.0 -v filesrc location=/home/root/\
philips_air_show-DWEU.mkv ! \
matroskademux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! \
video/x-raw,format=NV12,width=1920,height=1080,framerate=30000/1001 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=36 render-rectangle="<0,1080,1920,1080>"
```

- File #4
```bash
gst-launch-1.0 -v filesrc location=/home/root/\
03_caminandes_llamigos_1080p.mp4 ! \
qtdemux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! \
video/x-raw,format=NV12,width=1920,height=1080,framerate=24/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=37 render-rectangle="<1920,1080,1920,1080>"
```

#### Combined Pipeline
```bash
gst-launch-1.0 -v filesrc location=/home/root/\
Bosphorus_1920x1080_30fps_420_8bit_AVC_MP4.mp4 ! \
qtdemux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! \
video/x-raw,format=NV12,width=1920,height=1080,framerate=30/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=34 render-rectangle="<960,540,1920,1080>" & \
\
\
gst-launch-1.0 -v filesrc location=/home/root/\
sintel_trailer-720p.mp4 ! \
qtdemux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! \
video/x-raw,format=NV12,width=1280,height=720,framerate=24/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=35 render-rectangle="<2240,180,1280,720>" & \
\
\
gst-launch-1.0 -v filesrc location=/home/root/\
philips_air_show-DWEU.mkv ! \
matroskademux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! \
video/x-raw,format=NV12,width=1920,height=1080,framerate=30000/1001 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=36 render-rectangle="<0,1080,1920,1080>" & \
\
\
gst-launch-1.0 -v filesrc location=/home/root/\
03_caminandes_llamigos_1080p.mp4 ! \
qtdemux name=videodemux \
videodemux.video_0 ! \
h264parse ! \
omxh264dec ! \
video/x-raw,format=NV12,width=1920,height=1080,framerate=24/1 ! \
queue ! \
kmssink bus-id="b0140000.v_mix" plane-id=37 render-rectangle="<1920,1080,1920,1080>" &
```

- Stop playback pipelines by killing all background processes in the shell
```bash
kill $(jobs -p)
```