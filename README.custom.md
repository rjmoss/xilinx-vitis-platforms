# Xilinx Vitis Platforms

Personal collection of customized Vitis Embedded Platform recipes.

## References
- Platforms based on the Xilinx Official Platforms provided in the Xilinx [Vitis Embedded Platform Source](https://github.com/Xilinx/Vitis_Embedded_Platform_Source) repository.

## Quickstart Full Build Flow
Examples using the v2021.2 [xilinx_zcu106_base](./v2021.2/xilinx_zcu106_base) platform.

### Set build configuration variables
- Build variables are set in ```config/buildvars.sh```
- You may modify the variable values in this file, or pass new values from the command line during execution of the build
- Variables with a preceeding colon (example: ```: ${PRE_SYNTH:=TRUE}```) can be overridden on the command line
- Key variables are:
    - ```PRE_SYNTH```
        - Set to ```FALSE``` to implement the Vivado design and generate a bitstream
        - Set to ```TRUE``` (default) to use a pre-synthesis design to create the Vitis Platform

### Set Petalinux BSP configuration variables
- The Petalinux BSP configuration file ```config/petalinuxconfigs.sh``` can be used to override default Yocto and Petalinux configuration parameters at build time.
- Key Yocto Configurations Set by default:
    - ```CONFIG_TMP_DIR_LOCATION```
        - Set the working build folder for Yocto/Petalinux
        - Defaults to the common ```${SW_DIR}``` variable used by hardware and platform stages of the build process
    - ```CONFIG_SUBSYSTEM_TFTPBOOT_DIR```
        - Set the TFTP server folder where Petalinux copies build artifacts
        - Defaults to ```/srv/tftpbot/${PETALINUX_VER}/${PLATFORM}```
    - ```CONFIG_YOCTO_BB_NUMBER_THREADS```
        - Set the number of processor threads bitbake can use during a build
        - Defaults to ```16```
    - ```CONFIG_YOCTO_PARALLEL_MAKE```
        - Set the number of parallel make instances supported
        - Defaults to ```8```
    - ```CONFIG_YOCTO_LOCAL_SSTATE_FEEDS_URL```
        - Set the path to a local copy of the state/package cache provided by Xilinx
        - Defaults to ```/xilinx/local/sstate-mirrors/sstate_aarch64_${PETALINUX_VER}/aarch64/```

- Key Petalinux Configurations Set by default:
    - ```DL_DIR```
        - Set a common download location for petalinux to store downloaded files during image generation
        - Defaults to ```/xilinx/local/downloads/${PETALINUX_VER}```
    - ```SOURCE_MIRROR_URL```
        - Set to a common download location for package source files during bitbake recipe source fetch
        - Defaults to the same value as ```DL_DIR```, or ```/xilinx/local/downloads/${PETALINUX_VER}```
    - ```SSTATE_DIR```
        - Set to a common state cache location for resuse across builds and platforms
        - Defaults to ```/xilinx/local/sstate-cache/${PETALINUX_VER}```

### Build Sequence for using Custom Linux Images
0. Set the top level directory
1. Generate the Vivado Hardware Design
2. Apply software configurations
    - Apply custom bsp configuration (set in ```petalinuxconfigs.sh```)
    - Import the Vivado design configuration (```--get-hw-description```)
    - Configure the Linux Kernel
    - Configure the Rootfs

3. Build the Software (Petalinux BSP)
4. Build the Vitis Platform
5. Show Platform Information
6. Optional: Generate target boot images
7. Optional: Show generated image information
8. Optional: Build a custom SDK and SYSROOT for software development

#### Makefile Overview for the Custom flow
- The custom make sequence overview
```bash
$ source setenv.sh
$ make all PRE_SYNTH=FALSE
    ...
    all: hw sw_config sw platform pfminfo imageinfo

    hw:
        ${SCRIPT_DIR}/build_hw.sh

    sw_config: config_bsp config_xsa config_kernel config_rootfs

    config_bsp:
        ${SCRIPT_DIR}/config_bsp.sh

    config_xsa:
        ${SCRIPT_DIR}/config_xsa.sh

    config_kernel:
        ${SCRIPT_DIR}/config_kernel.sh

    config_rootfs:
        ${SCRIPT_DIR}/config_rootfs.sh

    sw:
        ${SCRIPT_DIR}/build_sw.sh
        ${SCRIPT_DIR}/copy_sw_components.sh

    platform:
        ${SCRIPT_DIR}/copy_platform_components.sh
        ${SCRIPT_DIR}/build_platform.sh

    pfminfo:
        ${SCRIPT_DIR}/show_platform_info.sh

    imageinfo:
        ${SCRIPT_DIR}/show_image_info.sh
```

- Build the boot image for the target
```bash
$ make bootimage
    ...
    bootimage:
        ${SCRIPT_DIR}/build_bootimage.sh
        ${SCRIPT_DIR}/copy_bootimage.sh
```

- Generate a custom SDK to support sotware development
```bash
$ make sdk
    ...
    sdk:
        ${SCRIPT_DIR}/build_sdk.sh
        ${SCRIPT_DIR}/show_sdk_info.sh
```

- Generate a custom SYSROOT to support software development
```bash
$ make sysroot
    ...
    sysroot:
        ${SCRIPT_DIR}/build_sysroot.sh
        ${SCRIPT_DIR}/show_sysroot_info.sh
```

#### Manual Build Walkthrough
0. Set the top level directory
```bash
$ source setenv.sh
    ################################################
    # setenv.sh: Setting Top Level Directory
    # TOP_DIR=/home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base
    # setenv.sh: Setting Script Directory
    # SCRIPT_DIR=/home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/scripts
    ################################################
```

1. Generate the Vivado Hardware Design
```bash
$ make hw PRE_SYNTH=FALSE
    ${SCRIPT_DIR}/build_hw.sh
    ################################################
    # build_hw.sh: Building hardware project
    ################################################

    ****** Vivado v2021.2 (64-bit)
      **** SW Build 3367213 on Tue Oct 19 02:47:39 MDT 2021
      **** IP Build 3369179 on Thu Oct 21 08:25:16 MDT 2021
        ** Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.

    source xsa_scripts/xsa.tcl -notrace
    TCL SCRIPT ARGUMENTS:
    ---------------------
    PLATFORM      = xilinx_zcu106_base
    PLATFORM_NAME = xilinx_zcu106_base_2021_2
    BOARD_NAME    = zcu106
    VER           = 2021.2
    PRE_SYNTH     = FALSE
    EMU_SUPPORT   = TRUE
    ---------------------
    PATHS:
    ---------------------
    BUILD_DIR     = build
    CONSTRS_DIR   = constrs
    IP_DIR        = ip
    ---------------------
    ...
    INFO: [Vivado 12-4896] Successfully created Hardware Platform: /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/hw/build/hw.xsa
write_hw_platform: Time (s): cpu = 00:00:13 ; elapsed = 00:00:15 . Memory (MB): peak = 4155.348 ; gain = 71.906 ; free physical = 36598 ; free virtual = 294994
    INFO: [Common 17-206] Exiting Vivado at Tue Mar 15 10:40:40 2022...
```

2. Apply software configurations
- To run all software configuration stages:
```bash
$ make sw_config
```

- Or, run each stage of configuration individually
- Apply custom bsp configuration (set in ```petalinuxconfigs.sh```)
```bash
$ make config_bsp
    ${SCRIPT_DIR}/config_bsp.sh
    ################################################
    # config_bsp.sh: Configuring BSP
    ################################################
    ################################################
    # Update: 1 of 8
    ################################################
    # update_plnx_config.sh
    ################################################
            File      : config
            Location  : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/project-spec/configs
            Key       : CONFIG_TMP_DIR_LOCATION
            OLD value : CONFIG_TMP_DIR_LOCATION="/home/xilinx/repositories/gitlab/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/build"
            NEW value : CONFIG_TMP_DIR_LOCATION="/home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/build"
    ################################################
    # Update: 2 of 8
    ################################################
    # update_plnx_config.sh
    ################################################
            File      : config
            Location  : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/project-spec/configs
            Key       : CONFIG_SUBSYSTEM_TFTPBOOT_DIR
            OLD value : CONFIG_SUBSYSTEM_TFTPBOOT_DIR="/srv/tftpboot/v2021_2/xilinx_zcu106_base"
            NEW value : CONFIG_SUBSYSTEM_TFTPBOOT_DIR="/srv/tftpboot/v2021.2/xilinx_zcu106_base"
    ################################################
    # Update: 3 of 8
    ################################################
    # update_plnx_config.sh
    ################################################
            File      : config
            Location  : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/project-spec/configs
            Key       : CONFIG_YOCTO_BB_NUMBER_THREADS
            OLD value : CONFIG_YOCTO_BB_NUMBER_THREADS="16"
            NEW value : CONFIG_YOCTO_BB_NUMBER_THREADS="16"
    ################################################
    # Update: 4 of 8
    ################################################
    # update_plnx_config.sh
    ################################################
            File      : config
            Location  : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/project-spec/configs
            Key       : CONFIG_YOCTO_PARALLEL_MAKE
            OLD value : CONFIG_YOCTO_PARALLEL_MAKE="8"
            NEW value : CONFIG_YOCTO_PARALLEL_MAKE="8"
    ################################################
    # Update: 5 of 8
    ################################################
    # update_plnx_config.sh
    ################################################
            File      : config
            Location  : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/project-spec/configs
            Key       : CONFIG_YOCTO_LOCAL_SSTATE_FEEDS_URL
            OLD value : CONFIG_YOCTO_LOCAL_SSTATE_FEEDS_URL="/xilinx/local/sstate-mirrors/sstate_aarch64_2021.2/aarch64/"
            NEW value : CONFIG_YOCTO_LOCAL_SSTATE_FEEDS_URL="/xilinx/local/sstate-mirrors/sstate_aarch64_2021.2/aarch64/"
    ################################################
    # Update: 6 of 8
    ################################################
    # update_plnx_config.sh
    ################################################
            File      : petalinuxbsp.conf
            Location  : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/project-spec/meta-user/conf
            Key       : DL_DIR
            OLD value : DL_DIR="/xilinx/local/downloads/2021.2"
            NEW value : DL_DIR="/xilinx/local/downloads/2021.2"
    ################################################
    # Update: 7 of 8
    ################################################
    # update_plnx_config.sh
    ################################################
            File      : petalinuxbsp.conf
            Location  : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/project-spec/meta-user/conf
            Key       : SOURCE_MIRROR_URL
            OLD value : SOURCE_MIRROR_URL="file:///xilinx/local/downloads/2021.2"
            NEW value : SOURCE_MIRROR_URL="file:///xilinx/local/downloads/2021.2"
    ################################################
    # Update: 8 of 8
    ################################################
    # update_plnx_config.sh
    ################################################
            File      : petalinuxbsp.conf
            Location  : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/project-spec/meta-user/conf
            Key       : SSTATE_DIR
            OLD value : SSTATE_DIR="/xilinx/local/sstate-cache/2021.2"
            NEW value : SSTATE_DIR="/xilinx/local/sstate-cache/2021.2"
```

- Import the Vivado design configuration (```--get-hw-description```)
```bash
$ make config_xsa
    ${SCRIPT_DIR}/config_xsa.sh
    ################################################
    # config_xsa.sh: Import hardware design
    ################################################
    [INFO] Sourcing buildtools
    INFO: Getting hardware description...
    INFO: Renaming hw.xsa to system.xsa
    [INFO] Generating Kconfig for project
    [INFO] Silentconfig project
    [INFO] Sourcing buildtools extended
    [INFO] Extracting yocto SDK to components/yocto. This may take time!
    [INFO] Sourcing build environment
    [INFO] Generating kconfig for Rootfs
    [INFO] Silentconfig rootfs
    [INFO] Generating plnxtool conf
    [INFO] Adding user layers
    [INFO] Generating workspace directory
```

- Configure the Linux Kernel
```bash
$ make config_kernel
    ${SCRIPT_DIR}/config_kernel.sh
    ################################################
    # config_kernel.sh: Configuring Kernel
    ################################################
    [INFO] Sourcing buildtools
    [INFO] Silentconfig project
    [INFO] Sourcing buildtools extended
    [INFO] Sourcing build environment
    [INFO] Generating kconfig for Rootfs
    [INFO] Silentconfig rootfs
    [INFO] Generating plnxtool conf
    [INFO] Generating workspace directory
    [INFO] Configuring: kernel
    [INFO] bitbake virtual/kernel -c cleansstate
    ...
    Sstate summary: Wanted 105 Found 104 Missed 1 Current 0 (99% match, 0% complete)
    NOTE: Executing Tasks
    NOTE: Tasks Summary: Attempted 474 tasks of which 463 didn't need to be rerun and all succeeded.
    [INFO] Successfully configured kernel
```

- Configure the Rootfs
```bash
$ make config_rootfs
    ${SCRIPT_DIR}/config_rootfs.sh
    ################################################
    # config_rootfs.sh: Configuring Rootfs
    ################################################
    [INFO] Sourcing buildtools
    [INFO] Silentconfig project
    [INFO] Generating kconfig for Rootfs
    [INFO] Silentconfig rootfs
    [INFO] Generating plnxtool conf
    [INFO] Successfully configured rootfs
```

3. Build the Software (Petalinux BSP)
- Execute the Petalinux Build
```bash
$ make sw
${SCRIPT_DIR}/build_sw.sh
    ################################################
    # build_sw.sh: Building software project
    ################################################
    [INFO] Sourcing buildtools
    [INFO] Building project
    [INFO] Sourcing buildtools extended
    [INFO] Sourcing build environment
    [INFO] Generating workspace directory
    INFO: bitbake petalinux-image-minimal
    NOTE: Started PRServer with DBfile: /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/build/cache/prserv.sqlite3, IP: 127.0.0.1, PORT: 38545, PID: 416902
    Loading cache: 100% |############################################################################################################################################| Time: 0:00:00
    Loaded 5125 entries from dependency cache.
    Parsing recipes: 100% |##########################################################################################################################################| Time: 0:00:00
    Parsing of 3476 .bb files complete (3468 cached, 8 parsed). 5133 targets, 232 skipped, 0 masked, 0 errors.
    NOTE: Resolving any missing task queue dependencies
    Initialising tasks: 100% |#######################################################################################################################################| Time: 0:00:14
    Checking sstate mirror object availability: 100% |###############################################################################################################| Time: 0:00:02
    Sstate summary: Wanted 3088 Found 2710 Missed 378 Current 87 (87% match, 88% complete)
    NOTE: Executing Tasks
    NOTE: Tasks Summary: Attempted 8908 tasks of which 7081 didn't need to be rerun and all succeeded.
    INFO: Successfully copied built images to tftp dir: /srv/tftpboot/v2021.2/xilinx_zcu106_base
    [INFO] Successfully built project
    ${SCRIPT_DIR}/copy_sw_components.sh
    ################################################
    # copy_sw_components.sh: Copy software components
    ################################################
```

4. Build the Vitis Platform
```bash
$ make platform
    ${SCRIPT_DIR}/copy_platform_components.sh
    ################################################
    # copy_platform_components.sh: Copy platform components
    ################################################
    ${SCRIPT_DIR}/build_platform.sh
    ################################################
    # build_platform.sh: Building platform files
    ################################################
    xsa_path      : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/hw/build/hw.xsa
    platform_name : xilinx_zcu106_base_2021_2
    platform_desc : A basic platform targeting the ZCU106 evaluation board.  More information at https://www.xilinx.com/products/boards-and-kits/zcu106.html
    emu_xsa_path  : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/hw/build/hw_emu/hw_emu.xsa
    platform_out  : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/platform_repo
    boot_dir_path : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/build/platform/boot
    img_dir_path  : /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/build/platform/image
    Opening the hardware design, this may take few seconds.
    INFO: [Hsi 55-2053] elapsed time for repository (/opt/tools/Xilinx/Vitis/2021.2/data/embeddedsw) loading 2 seconds
    INFO: Populating the default qemu data for the domain "xrt" from the install location /opt/tools/Xilinx/Vitis/2021.2/data/emulation/platforms/zynqmp/sw/a53_linux/qemu/
```

5. Show Platform Information
```bash
$ make pfminfo
    ${SCRIPT_DIR}/show_platform_info.sh
    ################################################
    # show_platform_info.sh: Showing Platform Information
    ################################################
    ==========================
    Basic Platform Information
    ==========================
    Platform:           xilinx_zcu106_base_2021_2
    File:               /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/platform_repo/xilinx_zcu106_base_2021_2/export/xilinx_zcu106_base_2021_2/xilinx_zcu106_base_2021_2.xpfm
    Description:        
    A basic platform targeting the ZCU106 evaluation board.  More information at https://www.xilinx.com/products/boards-and-kits/zcu106.html
        

    =====================================
    Hardware Platform (Shell) Information
    =====================================
    Vendor:                           xilinx.com
    Board:                            xilinx_zcu106_base_2021_2
    Name:                             xilinx_zcu106_base_2021_2
    Version:                          2021.2
    Generated Version:                2021.2
    Hardware:                         1
    Software Emulation:               1
    Hardware Emulation:               1
    Hardware Emulation Platform:      0
    FPGA Family:                      zynquplus
    FPGA Device:                      xczu7ev
    Board Vendor:                     xilinx.com
    Board Name:                       xilinx.com:zcu106:2.6
    Board Part:                       xczu7ev-ffvc1156-2-e

    =================
    Clock Information
    =================
      Default Clock Index: 0
      Clock Index:         0
        Frequency:         149.985000
      Clock Index:         1
        Frequency:         299.970000
      Clock Index:         2
        Frequency:         74.992500
      Clock Index:         3
        Frequency:         99.990000
      Clock Index:         4
        Frequency:         199.980000
      Clock Index:         5
        Frequency:         399.960000
      Clock Index:         6
        Frequency:         599.940000

    ==================
    Memory Information
    ==================
      Bus SP Tag: HP0
      Bus SP Tag: HP1
      Bus SP Tag: HP2
      Bus SP Tag: HP3
      Bus SP Tag: HPC0
      Bus SP Tag: HPC1
      Bus SP Tag: LPD

    =============================
    Software Platform Information
    =============================
    Number of Runtimes:            1
    Default System Configuration:  xilinx_zcu106_base_2021_2
    System Configurations:
      System Config Name:                      xilinx_zcu106_base_2021_2
      System Config Description:               xilinx_zcu106_base_2021_2
      System Config Default Processor Group:   xrt
      System Config Default Boot Image:        standard
      System Config Is QEMU Supported:         1
      System Config Processor Groups:
        Processor Group Name:      xrt
        Processor Group CPU Type:  cortex-a53
        Processor Group OS Name:   linux
      System Config Boot Images:
        Boot Image Name:           standard
        Boot Image Type:           
        Boot Image BIF:            xilinx_zcu106_base_2021_2/boot/linux.bif
        Boot Image Data:           xilinx_zcu106_base_2021_2/xrt/image
        Boot Image Boot Mode:      sd
        Boot Image RootFileSystem: 
        Boot Image Mount Path:     /mnt
        Boot Image Read Me:        xilinx_zcu106_base_2021_2/boot/generic.readme
        Boot Image QEMU Args:      xilinx_zcu106_base_2021_2/qemu/pmu_args.txt:xilinx_zcu106_base_2021_2/qemu/qemu_args.txt
        Boot Image QEMU Boot:      
        Boot Image QEMU Dev Tree:  
    Supported Runtimes:
      Runtime: OpenCL
```

6. Optional: Generate target boot images
```bash
$ make bootimage
    ${SCRIPT_DIR}/build_bootimage.sh
    ################################################
    # build_bootimage.sh: Building boot image
    ################################################
    [INFO] Sourcing buildtools
    INFO: Getting system flash information...
    INFO: File in BOOT BIN: "/home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/images/linux/fsbl.elf"
    INFO: File in BOOT BIN: "/home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/images/linux/pmufw.elf"
    INFO: File in BOOT BIN: "/home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/project-spec/hw-description/hw.bit"
    INFO: File in BOOT BIN: "/home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/images/linux/bl31.elf"
    INFO: File in BOOT BIN: "/home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/images/linux/system.dtb"
    INFO: File in BOOT BIN: "/home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/images/linux/u-boot.elf"
    INFO: Generating zynqmp binary package BOOT.BIN...


    ****** Xilinx Bootgen v2021.2
      **** Build date : Sep 30 2021-06:08:18
        ** Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.


    [INFO]   : Bootimage generated successfully

    INFO: Binary is ready.
    ${SCRIPT_DIR}/copy_bootimage.sh
    ################################################
    # copy_bootimage.sh: Copy boot image
    ################################################
```

7. Optional: Show generated image information
```bash
$ make imageinfo
    ${SCRIPT_DIR}/show_image_info.sh
    ################################################
    # show_image_info.sh: Showing Image Information
    ################################################
    /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/build/platform
    ├── boot
    │ ├── bl31.elf
    │ ├── BOOT.BIN
    │ ├── fsbl.elf
    │ ├── pmufw.elf
    │ ├── system.dtb
    │ └── u-boot.elf
    ├── filesystem
    │ ├── rootfs.ext4
    │ └── rootfs.tar.gz
    └── image
        ├── boot.scr
        ├── Image
        └── system.dtb

    3 directories, 11 files
```

8. Optional: Build a custom SDK and SYSROOT for software development
- Generate a custom SDK to support sotware development
```bash
$ make sdk
    ${SCRIPT_DIR}/build_sdk.sh
    ################################################
    # build_sdk.sh: Building Custom SDK
    ################################################
    [INFO] Sourcing buildtools
    [INFO] Building project
    [INFO] Sourcing buildtools extended
    [INFO] Sourcing build environment
    [INFO] Generating workspace directory
    INFO: bitbake petalinux-image-minimal -c do_populate_sdk
    NOTE: Started PRServer with DBfile: /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/build/cache/prserv.sqlite3, IP: 127.0.0.1, PORT: 33959, PID: 969789
    Loading cache: 100% |############################################################################################################################################| Time: 0:00:00
    Loaded 5125 entries from dependency cache.
    Parsing recipes: 100% |##########################################################################################################################################| Time: 0:00:00
    Parsing of 3476 .bb files complete (3468 cached, 8 parsed). 5133 targets, 232 skipped, 0 masked, 0 errors.
    NOTE: Resolving any missing task queue dependencies
    Initialising tasks: 100% |#######################################################################################################################################| Time: 0:00:03
    Checking sstate mirror object availability: 100% |###############################################################################################################| Time: 0:00:03
    Sstate summary: Wanted 758 Found 358 Missed 400 Current 1674 (47% match, 83% complete)
    NOTE: Executing Tasks
    NOTE: Tasks Summary: Attempted 6951 tasks of which 6932 didn't need to be rerun and all succeeded.
    [INFO] Copying SDK Installer...
    [INFO] Successfully built project
```

- Generate a custom SYSROOT to support software development
```bash
$ make sysroot
    ${SCRIPT_DIR}/build_sysroot.sh
    ################################################
    # build_sysroot.sh: Building Custom Sysroot
    ################################################
    PetaLinux SDK installer version 2021.2
    ======================================
    You are about to install the SDK to "/home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/sysroot". Proceed [Y/n]? Y
    Extracting SDK........................................................................................................................................................................................done
    Setting it up...done
    SDK has been successfully set up and is ready to be used.
    Each time you wish to use the SDK in a new shell session, you need to source the environment setup script e.g.
     $ . /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/sysroot/environment-setup-cortexa72-cortexa53-xilinx-linux
     
    ${SCRIPT_DIR}show_sysroot_info.sh
    ################################################
    # show_sysroot_info.sh: Showing SYSROOT Location
    ################################################
    /home/xilinx/tmp/xilinx-vitis-platforms/v2021.2/xilinx_zcu106_base/sw/petalinux/sysroot
    ├── environment-setup-cortexa72-cortexa53-xilinx-linux
    ├── site-config-cortexa72-cortexa53-xilinx-linux
    ├── sysroots
    └── version-cortexa72-cortexa53-xilinx-linux

    1 directory, 3 files
```